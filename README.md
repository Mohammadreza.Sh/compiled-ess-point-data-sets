Compiled Earth System Science Point Datasets
================
Created and maintained by: OpenGeoHub foundation
(<a href="mailto:tom.hengl@OpenGeoHub.org" class="email">tom.hengl@OpenGeoHub.org</a>)
8/24/2020



-   [![alt text](tex/R_logo.svg.png "About") About](#alt-text-about)
    -   [Overview](#overview)
    -   [Data license](#data-license)
    -   [Other similar repositories /
        projects](#other-similar-repositories-projects)
    -   [Machine Learning-ready data](#machine-learning-ready-data)
-   [![alt text](tex/R_logo.svg.png "Data sets") Data
    sets](#alt-text-data-sets)
    -   [Relief / geology](#relief-geology)
    -   [Land cover, land use and administrative
        data](#land-cover-land-use-and-administrative-data)
    -   [Vegetation indices](#vegetation-indices)
    -   [Land degradation indices](#land-degradation-indices)
    -   [Climatic and meteo data](#climatic-and-meteo-data)
    -   [Soil properties and classes](#soil-properties-and-classes)
    -   [Potential natural vegetation](#potential-natural-vegetation)
    -   [Hydrology and water dynamics](#hydrology-and-water-dynamics)
-   [References](#references)

[<img src="tex/opengeohub_logo_ml.png" alt="OpenGeoHub logo" width="350"/>](https://opengeohub.org)

![alt text](tex/R_logo.svg.png "About") About
---------------------------------------------

#### Overview

**Global Earth System Science Training Datasets** repository contains
compiled and analysis ready point (spatial) data sets that can be used
for building global spatial prediction models or similar analysis. It is
has been developed within the www.OpenLandMap.org project and its main
objective is [**to support a more reproducible and a more collaborative
global modeling and analysis of environmental and Earth System Science
variables**](https://opengeohub.org/contribute-training-data-landgis),
with especial focus on meteorological variables (Sparks, Hengl, &
Nelson, [2017](#ref-sparks2017gsodr)), land cover and vegetation
observations (Fritz et al., [2017](#ref-Fritz2017SD); Hengl et al.,
[2018](#ref-hengl2018global)), soil variables (Batjes et al.,
[2017](#ref-batjes2017wosis); Hengl et al.,
[2017](#ref-hengl2017soilgrids250m)) and measurements of fluxes and
ecosystem services (Jian et al., [2020](#ref-essd-2020-136); Kulmala,
[2018](#ref-kulmala2018build); Ukkola, Haughton, De Kauwe, Abramowitz, &
Pitman, [2017](#ref-ukkola2017fluxnetlsm)).

We bind and *clean* the data using R packages including
[plyr](http://had.co.nz/plyr), [dplyr](https://dplyr.tidyverse.org/) and
other tidyverse.org packages. We closely follow the specifications of
various data standards including the the
<a href="https://www.r-spatial.org/" class="uri">https://www.r-spatial.org/</a>
protocols and standards (Bivand, Pebesma, & Gomez-Rubio,
[2013](#ref-asdar); Wickham & Grolemund, [2017](#ref-r4ds)), then export
data into R and [OpenML.org foundation](https://docs.openml.org/)
(Casalicchio et al., [2017](#ref-casalicchio2017openml)) compatible
formats to make the data more usable for modeling purposes. The
visualizations below are based on using the [Interrupted Goode
homolosine](https://wilkelab.org/practicalgg/articles/goode.html)
projection (an equal area projection for the global land mask).

#### Data license

If not specified otherwise, www.OpenLandMap.org produces open data,
licensed under the [Open Data Commons Open Database License
(ODbL)](https://opendatacommons.org/licenses/odbl/) and/or [Creative
Commons Attribution-ShareAlike 4.0 International license (CC
BY-SA)](https://creativecommons.org/licenses/by-sa/4.0/legalcode).

Learn more about this repository and how to contribute:

-   [OpenLandMap.org project
    backgrounds](https://opengeohub.org/about-landgis),
-   How to download and load the data,
-   [How to submit a new data
    set](https://opengeohub.org/contribute-training-data-openlandmap),
-   [Report an issue or
    bug](https://gitlab.com/openlandmap/compiled-ess-point-data-sets/-/issues),

#### Other similar repositories / projects

Global station data / monitoring projects of interest:

-   [Degree Confluence project](http://confluence.org/),
-   [FLUXNET global network](https://fluxnet.fluxdata.org/),
-   [Global Biodiversity Information Facility (GBIF)
    data](https://www.gbif.org),
-   [Global soil respiration database
    (SRDB)](https://github.com/bpbond/srdb)
-   [Geo-wiki data sets](https://www.geo-wiki.org/pages/data),
-   [International Soil Moisture
    Network](https://ismn.geo.tuwien.ac.at/en/),
-   [International Soil Carbon Network](http://iscn.fluxdata.org/),
-   [LandPKS project](http://portal.landpotential.org/#/landpksmap),
-   [NASA Global Landslide Catalog
    (GLC)](https://data.nasa.gov/Earth-Science/Global-Landslide-Catalog/h9d8-neg4),
-   [NOAA Global Surface Summary of the Day -
    GSOD](https://catalog.data.gov/dataset/global-surface-summary-of-the-day-gsod),
-   [USGS Global Earthquake Hazards
    Database](https://earthquake.usgs.gov/data/),
-   [WorldBank Catalog](https://datacatalog.worldbank.org/),
-   [WMO World Weather
    Watch](http://public.wmo.int/en/programmes/world-weather-watch),
-   [WMO Global Observatory
    System](http://public.wmo.int/en/programmes/global-observing-system),
-   [WRI Global Forest Watch
    Fires](https://fires.globalforestwatch.org/),

#### Machine Learning-ready data

Tables **classification-/** and/or **regression-matrix** contain values
of target variables plus all global covariate layers (available via
www.OpenLandMap.org) i.e. these are tables that can be directly used for
testing various machine learning algorithms. Test your own approaches
and algorithms and let us know if you discover some significant
relationship or pattern (read more in: [*“OpenLandMap: enabling Machine
Learning for Global
Good”*](https://opengeohub.org/openlandmap-using-machine-learning-global-good)).
Currently [available global
layers](https://zenodo.org/search?page=1&size=20&q=LandGIS) include
(Hengl & MacMillan, [2019](#ref-hengl2019predictive)):

-   DEM-derived surfaces — slope, profile curvature, Multiresolution
    Index of Valley Bottom Flatness (VBF), deviation from Mean Value,
    valley depth, negative and positive Topographic Openness and SAGA
    Wetness Index — all based on MERIT DEM (Yamazaki et al.,
    [2019](#ref-yamazaki2019merit)). All DEM derivatives were computed
    using SAGA GIS (Conrad et al., [2015](#ref-gmd-8-1991-2015)),

-   Long-term averaged monthly mean and standard deviation of the
    [FAPAR](https://land.copernicus.eu/global/products/fapar) based on
    the PROBA-V images,

-   Long-term averaged mean monthly surface reflectances for MODIS bands
    4 (NIR) and 7 (MIR). Derived using a stack of MCD43A4 images (Mira
    et al., [2015](#ref-mira2015modis)),

-   Long-term (2000-2018) averaged monthly mean and standard deviation
    of the MODIS land surface temperature (daytime and nighttime).
    Derived using a stack of MOD11A2 LST images (Wan,
    [2006](#ref-wan2006modis)),

-   Long-term (2000-2016) averaged mean monthly hours under snow cover
    based on a stack of MOD10A2 8-day snow occurrence images (Hall &
    Riggs, [2007](#ref-hall2007accuracy)),

-   Land cover classes (2016) based on the ESA’s land cover CCI project
    (Bontemps et al., [2013](#ref-bontemps2013consistent)),

-   Monthly precipitation images based on a weighted average between the
    SM2RAIN (Brocca et al., [2019](#ref-brocca2019sm2rain)), WorldClim
    monthly precipitation (Fick & Hijmans,
    [2017](#ref-fick2017worldclim)) and CHELSA climate (Karger et al.,
    [2017](#ref-karger2017climatologies)),

-   ESA’s long-term averaged mean monthly hours under [snow
    cover](http://maps.elie.ucl.ac.be/CCI/viewer/index.php). Derived
    using MOD10A2 8-day snow occurrence images,

-   Lithologic units (acid plutonics, acid volcanic, basic plutonics,
    basic volcanics, carbonate sedimentary rocks, evaporite, ice and
    glaciers, intermediate plutonics, intermediate volcanics,
    metamorphics, mixed sedimentary rocks, pyroclastics, siliciclastic
    sedimentary rocks, unconsolidated sediment) based on a Global
    Lithological Map GLiM (Hartmann & Moosdorf,
    [2012](#ref-GGGE:GGGE2352)),

-   Landform classes (breaks/foothills, flat plains, high mountains/deep
    canyons, hills, low hills, low mountains, smooth plains) based on
    the USGS’s Map of Global Ecological Land Units (Sayre et al.,
    [2014](#ref-sayre2014new)),

-   Global Water Table Depth in meters (Fan, Li, & Miguez-Macho,
    [2013](#ref-fan2013global)),

-   Landsat-based estimated distribution of Mangroves (Giri et al.,
    [2011](#ref-giri2011status)),

-   Average soil and sedimentary-deposit thickness in meters (Pelletier
    et al., [2016](#ref-Pelletier2016)).

**Disclaimer**: The data is provided “as is”. [OpenGeoHub
foundation](https://opengeohub.org/about) and its suppliers and
licensors hereby disclaim all warranties of any kind, express or
implied, including, without limitation, the warranties of
merchantability, fitness for a particular purpose and non-infringement.
Neither OpenGeoHub foundation nor its suppliers and licensors, makes any
warranty that the Website will be error free or that access thereto will
be continuous or uninterrupted. You understand that you download from,
or otherwise obtain content or services through, the Website at your own
discretion and risk.

![alt text](tex/R_logo.svg.png "Data sets") Data sets
-----------------------------------------------------

#### Relief / geology

-   Under construction,

#### Land cover, land use and administrative data

###### Import steps: [nat.landcover](themes/lcv/NativeLandCover/)

<img src="img/lcv_nat.landcover.pnts_sites.png" title="Native vegetation land cover field observations." alt="Native vegetation land cover field observations." width="100%" />

-   :computer: output RDS files:
    [`lcv_nat.landcover.pnts_sites.rds`](out/rds/lcv_nat.landcover.pnts_sites.rds),
-   :open\_file\_folder: data download:
    [10.5281/zenodo.3631253](https://doi.org/10.5281/zenodo.3631253),
-   :chart\_with\_upwards\_trend: classification-matrix:
    [`lcv_nat.landcover.pnts_sites_cm.rds`](out/rds/lcv_nat.landcover.pnts_sites_cm.rds),
-   :book: citation: Hengl, T., Jung, M., & Visconti, P., (2020).
    **Potential distribution of land cover classes (Potential Natural
    Vegetation) at 250 m spatial resolution (Version v0.1)** \[Data
    set\]. Zenodo.
    <a href="http://doi.org/10.5281/zenodo.3631253" class="uri">http://doi.org/10.5281/zenodo.3631253</a>,
-   :package: connected packages / projects: [Geo-wiki
    data](https://www.geo-wiki.org/pages/data), [Nature map
    Explorer](https://explorer.naturemap.earth/),

#### Vegetation indices

-   Under construction,

#### Land degradation indices

-   Under construction,

#### Climatic and meteo data

###### Import steps: gsod.daily

<img src="img/clm_gsod.pnts_sites.png" title="GSOD data set." alt="GSOD data set." width="100%" />

-   :computer: output RDS files: *pending*,
-   :open\_file\_folder: data download:
    [gov.noaa.ncdc:C00516](https://data.noaa.gov/dataset/dataset/global-surface-summary-of-the-day-gsod),
-   :book: citation: Sparks, A.H., Hengl, T., and Nelson, A. (2017).
    **GSODR: Global Summary Daily Weather Data in R**. The Journal of
    Open Source Software, 2(10).
    <a href="https://dx.doi.org/10.21105/joss.00177" class="uri">https://dx.doi.org/10.21105/joss.00177</a>
-   :package: connected packages / projects:
    [GSODR](https://github.com/ropensci/GSODR),
    [meteo](https://cran.r-project.org/package=meteo),

#### Soil properties and classes

###### Import steps: [soilchem](themes/sol/SoilChemDB/)

<img src="img/sol_chem.pnts_sites.png" title="Soil chemical and physical soil properties." alt="Soil chemical and physical soil properties." width="100%" />

-   :computer: output RDS files:
    [`sol_chem.pnts_horizons.rds`](out/rds/sol_chem.pnts_horizons.rds),
-   :chart\_with\_upwards\_trend: regression-matrix:
    [`sol_chem.pnts_horizons_rm.rds`](out/rds/sol_chem.pnts_horizons_rm.rds),
-   :open\_file\_folder: data download: NA,
-   :book: citation: NA
-   :package: connected packages / projects:
    [SOCDR](https://github.com/ISCN/SOCDRaHR2),
    [febr](https://github.com/febr-team/febr-package),
    [soilDB](https://cran.r-project.org/package=soilDB),
    [SoilHealthDB](https://doi.org/10.1038/s41597-020-0356-3),

###### Import steps: [soilhydro](themes/sol/SoilHydroDB/)

<img src="img/sol_hydro.pnts_sites.png" title="Soil hydraulic and physical soil properties." alt="Soil hydraulic and physical soil properties." width="100%" />

-   :computer: output RDS files:
    [`sol_hydro.pnts_horizons.rds`](out/rds/sol_hydro.pnts_horizons.rds),
    [`sol_ksat.pnts_horizons.rds`](out/rds/sol_ksat.pnts_horizons.rds),
-   :chart\_with\_upwards\_trend: regression-matrix:
    [`sol_hydro.pnts_horizons_rm.rds`](out/rds/sol_hydro.pnts_horizons_rm.rds),
-   :open\_file\_folder: data download:
    [10.5281/zenodo.3752721](https://doi.org/10.5281/zenodo.3752721),
-   :book: citation: Gupta, S., Hengl, T., Lehmann, P., Bonetti, S., and
    Or, D. **SoilKsatDB: global soil saturated hydraulic conductivity
    measurements for geoscience applications**. Earth Syst. Sci. Data
    Discuss.,
    <a href="https://doi.org/10.5194/essd-2020-149" class="uri">https://doi.org/10.5194/essd-2020-149</a>,
    in review, 2020.
-   :package: connected packages / projects:
    [HydroMe](https://cran.r-project.org/package=HydroMe),
    [soilphysics](https://cran.r-project.org/package=soilphysics),
    [soiltexture](https://cran.r-project.org/package=soiltexture),
    [soilDB](https://cran.r-project.org/package=soilDB),

#### Potential natural vegetation

###### Import steps: [Biomes](themes/pnv/Biomes/)

<img src="img/pnv_biomes.pnts_sites.png" title="Observed global Biomes (BIOME 6000 data set)." alt="Observed global Biomes (BIOME 6000 data set)." width="100%" />

-   :computer: output RDS files:
    [`pnv_biomes.pnts_sites.rds`](out/rds/pnv_biomes.pnts_sites.rds),
-   :chart\_with\_upwards\_trend: classification-matrix:
    [`pnv_biomes.pnts_sites_cm.rds`](out/rds/pnv_biomes.pnts_sites_cm.rds),
-   :open\_file\_folder: data download:
    [10.7910/DVN/QQHCIK](http://dx.doi.org/10.7910/DVN/QQHCIK),
-   :book: citation: Hengl T, Walsh MG, Sanderman J, Wheeler I, Harrison
    SP, Prentice IC. (2018). **Global mapping of potential natural
    vegetation: an assessment of machine learning algorithms for
    estimating land potential**. PeerJ 6:e5457.
    <a href="https://doi.org/10.7717/peerj.5457" class="uri">https://doi.org/10.7717/peerj.5457</a>
-   :package: connected packages / projects: [Biome 6000
    project](http://www.bridge.bris.ac.uk/projects/BIOME_6000),

#### Hydrology and water dynamics

-   Under construction,

References
----------

<div id="refs" class="references hanging-indent">

<div id="ref-batjes2017wosis">

Batjes, N. H., Ribeiro, E., Oostrum, A. van, Leenaars, J., Hengl, T., &
Jesus, J. M. de. (2017). WoSIS: Providing standardised soil profile data
for the world. *Earth System Science Data*, *9*(1), 1.
doi:[10.5194/essd-9-1-2017](https://doi.org/10.5194/essd-9-1-2017)

</div>

<div id="ref-asdar">

Bivand, R. S., Pebesma, E., & Gomez-Rubio, V. (2013). *Applied spatial
data analysis with R, second edition*. Springer, NY. Retrieved from
<http://www.asdar-book.org/>

</div>

<div id="ref-bontemps2013consistent">

Bontemps, S., Defourny, P., Radoux, J., Van Bogaert, E., Lamarche, C.,
Achard, F., … others. (2013). Consistent global land cover maps for
climate modelling communities: Current achievements of the esa’s land
cover cci. In *Proceedings of the esa living planet symposium,
edimburgh* (pp. 9–13).

</div>

<div id="ref-brocca2019sm2rain">

Brocca, L., Filippucci, P., Hahn, S., Ciabatta, L., Massari, C., Camici,
S., … Wagner, W. (2019). SM2RAIN–ascat (2007–2018): Global daily
satellite rainfall data from ascat soil moisture observations. *Earth
System Science Data*, *11*(4).

</div>

<div id="ref-casalicchio2017openml">

Casalicchio, G., Bossek, J., Lang, M., Kirchhoff, D., Kerschke, P.,
Hofner, B., … Bischl, B. (2017). OpenML: An R package to connect to the
machine learning platform OpenML. *Computational Statistics*, 1–15.
doi:[10.1007/s00180-017-0742-2](https://doi.org/10.1007/s00180-017-0742-2)

</div>

<div id="ref-gmd-8-1991-2015">

Conrad, O., Bechtel, B., Bock, M., Dietrich, H., Fischer, E., Gerlitz,
L., … Böhner, J. (2015). System for automated geoscientific analyses
(saga) v. 2.1.4. *Geoscientific Model Development*, *8*(7), 1991–2007.
doi:[10.5194/gmd-8-1991-2015](https://doi.org/10.5194/gmd-8-1991-2015)

</div>

<div id="ref-fan2013global">

Fan, Y., Li, H., & Miguez-Macho, G. (2013). Global patterns of
groundwater table depth. *Science*, *339*(6122), 940–943.

</div>

<div id="ref-fick2017worldclim">

Fick, S. E., & Hijmans, R. J. (2017). WorldClim 2: New 1-km spatial
resolution climate surfaces for global land areas. *International
Journal of Climatology*, *37*(12), 4302–4315.

</div>

<div id="ref-Fritz2017SD">

Fritz, S., See, L., Perger, C., McCallum, I., Schill, C., Schepaschenko,
D., … Obersteiner, M. (2017). A global dataset of crowdsourced land
cover and land use reference data. *Scientific Data*, *4*, 170075.
doi:[10.1038/sdata.2017.75](https://doi.org/10.1038/sdata.2017.75)

</div>

<div id="ref-giri2011status">

Giri, C., Ochieng, E., Tieszen, L. L., Zhu, Z., Singh, A., Loveland, T.,
… Duke, N. (2011). Status and distribution of mangrove forests of the
world using earth observation satellite data. *Global Ecology and
Biogeography*, *20*(1), 154–159.

</div>

<div id="ref-hall2007accuracy">

Hall, D. K., & Riggs, G. A. (2007). Accuracy assessment of the MODIS
snow products. *Hydrological Processes*, *21*(12), 1534–1547.

</div>

<div id="ref-GGGE:GGGE2352">

Hartmann, J., & Moosdorf, N. (2012). The new global lithological map
database GLiM: A representation of rock properties at the Earth surface.
*Geochemistry, Geophysics, Geosystems*, *13*(12), n/a–n/a.
doi:[10.1029/2012GC004370](https://doi.org/10.1029/2012GC004370)

</div>

<div id="ref-hengl2017soilgrids250m">

Hengl, T., Jesus, J. M. de, Heuvelink, G. B., Gonzalez, M. R.,
Kilibarda, M., Blagotić, A., … others. (2017). SoilGrids250m: Global
gridded soil information based on machine learning. *PLoS One*, *12*(2).
doi:[10.1371/journal.pone.0169748](https://doi.org/10.1371/journal.pone.0169748)

</div>

<div id="ref-hengl2019predictive">

Hengl, T., & MacMillan, R. A. (2019). *Predictive soil mapping with r*
(p. 370). Wageningen, the Netherlands: Lulu. com.

</div>

<div id="ref-hengl2018global">

Hengl, T., Walsh, M. G., Sanderman, J., Wheeler, I., Harrison, S. P., &
Prentice, I. C. (2018). Global mapping of potential natural vegetation:
An assessment of machine learning algorithms for estimating land
potential. *PeerJ*, *6*, e5457.
doi:[10.7717/peerj.5457](https://doi.org/10.7717/peerj.5457)

</div>

<div id="ref-essd-2020-136">

Jian, J., Vargas, R., Anderson-Teixeira, K., Stell, E., Herrmann, V.,
Horn, M., … Bond-Lamberty, B. (2020). A restructured and updated global
soil respiration database (srdb-v5). *Earth System Science Data
Discussions*, *2020*, 1–19.
doi:[10.5194/essd-2020-136](https://doi.org/10.5194/essd-2020-136)

</div>

<div id="ref-karger2017climatologies">

Karger, D. N., Conrad, O., Böhner, J., Kawohl, T., Kreft, H.,
Soria-Auza, R. W., … Kessler, M. (2017). Climatologies at high
resolution for the earth’s land surface areas. *Scientific Data*, *4*,
170122.

</div>

<div id="ref-kulmala2018build">

Kulmala, M. (2018). Build a global earth observatory. Nature Publishing
Group.
doi:[10.1038/d41586-017-08967-y](https://doi.org/10.1038/d41586-017-08967-y)

</div>

<div id="ref-mira2015modis">

Mira, M., Weiss, M., Baret, F., Courault, D., Hagolle, O.,
Gallego-Elvira, B., & Olioso, A. (2015). The modis (collection v006)
brdf/albedo product mcd43d: Temporal course evaluated over agricultural
landscape. *Remote Sensing of Environment*, *170*, 216–228.

</div>

<div id="ref-Pelletier2016">

Pelletier, J. D., Broxton, P. D., Hazenberg, P., Zeng, X., Troch, P. A.,
Niu, G.-Y., … Gochis, D. (2016). A gridded global data set of soil,
immobile regolith, and sedimentary deposit thicknesses for regional and
global land surface modeling. *Journal of Advances in Modeling Earth
Systems*, *8*.
doi:[10.1002/2015MS000526](https://doi.org/10.1002/2015MS000526)

</div>

<div id="ref-sayre2014new">

Sayre, R., Dangermond, J., Frye, C., Vaughan, R., Aniello, P., Breyer,
S., … others. (2014). *A new map of global ecological land units—an
ecophysiographic stratification approach*. Washington, DC: USGS /
Association of American Geographers.

</div>

<div id="ref-sparks2017gsodr">

Sparks, A., Hengl, T., & Nelson, A. (2017). GSODR: Global Summary Daily
Weather Data in R. *Journal of Open Source Software*, *2*(10), 177.

</div>

<div id="ref-ukkola2017fluxnetlsm">

Ukkola, A., Haughton, N., De Kauwe, M., Abramowitz, G., & Pitman, A.
(2017). FluxnetLSM R package (v1. 0): a community tool for processing
FLUXNET data for use in land surface modelling. *Geosci. Model Dev.*
doi:[10.5194/gmd-10-3379-2017](https://doi.org/10.5194/gmd-10-3379-2017)

</div>

<div id="ref-wan2006modis">

Wan, Z. (2006). *MODIS land surface temperature products users’ guide*
(p. 35). ICESS, University of California.

</div>

<div id="ref-r4ds">

Wickham, H., & Grolemund, G. (2017). *R for data science*. O’Reilly.
Retrieved from <http://r4ds.had.co.nz/>

</div>

<div id="ref-yamazaki2019merit">

Yamazaki, D., Ikeshima, D., Sosa, J., Bates, P. D., Allen, G. H., &
Pavelsky, T. M. (2019). MERIT hydro: A high-resolution global
hydrography map based on latest topography dataset. *Water Resources
Research*, *55*(6), 5053–5073.

</div>

</div>
