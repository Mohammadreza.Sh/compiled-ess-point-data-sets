Global compilation of soil chemical and physical properties
================
Tom Hengl
(<a href="mailto:tom.hengl@OpenGeoHub.org" class="email">tom.hengl@OpenGeoHub.org</a>)
8/24/2020



-   [Soil chemical and physical
    properties](#soil-chemical-and-physical-properties)
-   [![alt text](../../../tex/R_logo.svg.png "Packages in use")
    Specifications](#alt-text-specifications)
    -   [*Target variables:*](#target-variables)
-   [![alt text](../../../tex/R_logo.svg.png "Data import") Data
    import](#alt-text-data-import)
    -   [National Cooperative Soil Survey Characterization
        Database](#national-cooperative-soil-survey-characterization-database)
    -   [Africa soil profiles database](#africa-soil-profiles-database)
    -   [ISRIC WISE harmonized soil profile
        data](#isric-wise-harmonized-soil-profile-data)
    -   [GEMAS](#gemas)
    -   [LUCAS soil](#lucas-soil)
    -   [Mangrove forest soil DB](#mangrove-forest-soil-db)
    -   [CIFOR peatland points](#cifor-peatland-points)
    -   [LandPKS observations](#landpks-observations)
    -   [EGRPR](#egrpr)
    -   [Canada National Pedon DB](#canada-national-pedon-db)
    -   [Canadian upland forest soil profile and carbon stocks
        database](#canadian-upland-forest-soil-profile-and-carbon-stocks-database)
    -   [SOTER China soil profiles](#soter-china-soil-profiles)
    -   [SISLAC](#sislac)
    -   [FEBR](#febr)
    -   [Soil Profile DB for Costa
        Rica](#soil-profile-db-for-costa-rica)
    -   [Iran soil profile DB](#iran-soil-profile-db)
    -   [Northern circumpolar permafrost soil
        profiles](#northern-circumpolar-permafrost-soil-profiles)
    -   [CSIRO National Soil Site
        Database](#csiro-national-soil-site-database)
    -   [NAMSOTER](#namsoter)
    -   [Worldwide organic soil carbon and nitrogen
        data](#worldwide-organic-soil-carbon-and-nitrogen-data)
    -   [Interior Alaska Carbon and Nitrogen
        stocks](#interior-alaska-carbon-and-nitrogen-stocks)
    -   [Remnant native SOC database](#remnant-native-soc-database)
    -   [Soil Health DB](#soil-health-db)
    -   [Pseudo-observations](#pseudo-observations)
-   [![alt text](../../../tex/R_logo.svg.png "Bind everything") Bind
    everything](#alt-text-bind-everything)
    -   [Histogram plots](#histogram-plots)
    -   [RDS files](#rds-files)
-   [![alt
    text](../../../tex/R_logo.svg.png "Overlay www.OpenLandMap.org layers")
    Overlay with www.OpenLandMap.org
    layers](#alt-text-overlay-with-www.openlandmap.org-layers)

[<img src="../../../tex/opengeohub_logo_ml.png" alt="OpenGeoHub logo" width="350"/>](https://opengeohub.org)

Soil chemical and physical properties
-------------------------------------

Part of: [Compiled ESS point data
sets](https://gitlab.com/openlandmap/compiled-ess-point-data-sets) Last
update: 2020-09-01

Read more about soil chemical properties, global soil data sets and
functionality:

-   Batjes, N. H., Ribeiro, E., van Oostrum, A., Leenaars, J., Hengl,
    T., & de Jesus, J. M. (2017). [WoSIS: providing standardised soil
    profile data for the
    world](http://www.earth-syst-sci-data.net/9/1/2017/). Earth System
    Science Data, 9(1), 1.
-   de Sousa, D. F., Rodrigues, S., de Lima, H. V., & Chagas, L. T.
    (2020). [R software packages as a tool for evaluating soil physical
    and hydraulic
    properties](https://doi.org/10.1016/j.compag.2019.105077). Computers
    and Electronics in Agriculture, 168, 105077.
-   Arrouays, D., Leenaars, J. G., Richer-de-Forges, A. C., Adhikari,
    K., Ballabio, C., Greve, M., … & Heuvelink, G. (2017). [Soil legacy
    data rescue via GlobalSoilMap and other international and national
    initiatives](https://doi.org/10.1016/j.grj.2017.06.001). GeoResJ,
    14, 1-19.

![alt text](../../../tex/R_logo.svg.png "Packages in use") Specifications
-------------------------------------------------------------------------

-   Metadata information: [“Soil Survey Investigation Report
    No. 42.”](https://www.nrcs.usda.gov/Internet/FSE_DOCUMENTS/stelprdb1253872.pdf)
    and [“Soil Survey Investigation Report
    No. 45.”](https://www.nrcs.usda.gov/Internet/FSE_DOCUMENTS/nrcs142p2_052226.pdf),
-   Sample DB: [National Cooperative Soil Survey (NCSS) Soil
    Characterization
    Database](https://ncsslabdatamart.sc.egov.usda.gov/),

#### *Target variables:*

    site.names = c("site_key", "usiteid", "site_obsdate", "longitude_decimal_degrees", "latitude_decimal_degrees")
    hor.names = c("labsampnum","site_key","layer_sequence","hzn_top","hzn_bot","hzn_desgn", "tex_psda", "clay_tot_psa", "silt_tot_psa", "sand_tot_psa", "oc", "oc_d", "c_tot", "n_tot", "ph_kcl", "ph_h2o", "ph_cacl2", "cec_sum", "cec_nh4", "ecec", "wpg2", "db_od", "ca_ext", "mg_ext", "na_ext", "k_ext", "ec_satp", "ec_12pre")
    ## target structure:
    col.names = c("site_key", "usiteid", "site_obsdate", "longitude_decimal_degrees", "latitude_decimal_degrees", "labsampnum", "layer_sequence","hzn_top","hzn_bot","hzn_desgn", "tex_psda", "clay_tot_psa", "silt_tot_psa", "sand_tot_psa", "oc", "oc_d", "c_tot", "n_tot", "ph_kcl", "ph_h2o", "ph_cacl2", "cec_sum", "cec_nh4", "ecec", "wpg2", "db_od", "ca_ext", "mg_ext", "na_ext", "k_ext", "ec_satp", "ec_12pre", "source_db", "confidence_degree")

-   `clay_tot_psa`: Clay, Total in % wt for &lt;2 mm soil fraction,
-   `silt_tot_psa`: Silt, Total in % wt for &lt;2 mm soil fraction,
-   `sand_tot_psa`: Sand, Total in % wt for &lt;2 mm soil fraction,
-   `oc`: Carbon, Organic in g/kg for &lt;2 mm soil fraction,
-   `oc_d`: Soil organic carbon density in kg/m3,
-   `c_tot`: Carbon, Total in g/kg for &lt;2 mm soil fraction,
-   `n_tot`: Nitrogen, Total NCS in g/kg for &lt;2 mm soil fraction,
-   `ph_kcl`: pH, KCl Suspension for &lt;2 mm soil fraction,
-   `ph_h2o`: pH, 1:1 Soil-Water Suspension for &lt;2 mm soil fraction,
-   `ph_cacl2`: pH, CaCl2 Suspension for &lt;2 mm soil fraction,
-   `cec_sum`: Cation Exchange Capacity, Summary, in cmol(+)/kg for
    &lt;2 mm soil fraction,
-   `cec_nh4`: Cation Exchange Capacity, NH4 prep, in cmol(+)/kg for
    &lt;2 mm soil fraction,
-   `ecec`: Cation Exchange Capacity, Effective, CMS derived value
    default, standa prep in cmol(+)/kg for &lt;2 mm soil fraction,
-   `wpg2`: Coarse fragments in % wt for &gt;2 mm soil fraction,
-   `db_od`: Bulk density (Oven Dry) in g/cm3 (4A1h),
-   `ca_ext`: Calcium, Extractable in mg/kg for &lt;2 mm soil fraction
    (usually Mehlich3),
-   `mg_ext`: Magnesium, Extractable in mg/kg for &lt;2 mm soil fraction
    (usually Mehlich3),
-   `na_ext`: Sodium, Extractable in mg/kg for &lt;2 mm soil fraction
    (usually Mehlich3),
-   `k_ext`: Potassium, Extractable in mg/kg for &lt;2 mm soil fraction
    (usually Mehlich3),
-   `ec_satp`: Electrical Conductivity, Saturation Extract in dS/m for
    &lt;2 mm soil fraction,
-   `ec_12pre`: Electrical Conductivity, Predict, 1:2 (w/w) in dS/m for
    &lt;2 mm soil fraction,

![alt text](../../../tex/R_logo.svg.png "Data import") Data import
------------------------------------------------------------------

#### National Cooperative Soil Survey Characterization Database

-   National Cooperative Soil Survey, (2020). National Cooperative Soil
    Survey Characterization Database. Data download URL:
    <a href="http://ncsslabdatamart.sc.egov.usda.gov/" class="uri">http://ncsslabdatamart.sc.egov.usda.gov/</a>

This data set is continuously updated.

    if(!exists("chemsprops.NCSS")){
      ncss.site <- read.csv("/mnt/DATA/Soil_points/INT/USDA_NCSS/NCSS_Site_Location.csv", stringsAsFactors = FALSE)
      ncss.layer <- read.csv("/mnt/DATA/Soil_points/INT/USDA_NCSS/NCSS_Layer.csv", stringsAsFactors = FALSE)
      ncss.bdm <- read.csv("/mnt/DATA/Soil_points/INT/USDA_NCSS/NCSS_Bulk_Density_and_Moisture.csv", stringsAsFactors = FALSE)
      ## multiple measurements
      summary(as.factor(ncss.bdm$prep_code))
      ncss.bdm.0 <- ncss.bdm[ncss.bdm$prep_code=="S",]
      summary(ncss.bdm.0$db_od)
      ## 0 BD values --- error!
      ncss.carb <- read.csv("/mnt/DATA/Soil_points/INT/USDA_NCSS/NCSS_Carbon_and_Extractions.csv", stringsAsFactors = FALSE)
      ncss.organic <- read.csv("/mnt/DATA/Soil_points/INT/USDA_NCSS/NCSS_Organic.csv", stringsAsFactors = FALSE)
      ncss.pH <- read.csv("/mnt/DATA/Soil_points/INT/USDA_NCSS/NCSS_pH_and_Carbonates.csv", stringsAsFactors = FALSE)
      #str(ncss.pH)
      #summary(ncss.pH$ph_h2o)
      #summary(!is.na(ncss.pH$ph_h2o))
      ncss.PSDA <- read.csv("/mnt/DATA/Soil_points/INT/USDA_NCSS/NCSS_PSDA_and_Rock_Fragments.csv", stringsAsFactors = FALSE)
      ncss.CEC <- read.csv("/mnt/DATA/Soil_points/INT/USDA_NCSS/NCSS_CEC_and_Bases.csv")
      ncss.salt <- read.csv("/mnt/DATA/Soil_points/INT/USDA_NCSS/NCSS_Salt.csv")
      ncss.horizons <- plyr::join_all(list(ncss.bdm.0, ncss.layer, ncss.carb, ncss.organic[,c("labsampnum", "result_source_key", "c_tot", "n_tot", "db_od", "oc")], ncss.pH, ncss.PSDA, ncss.CEC, ncss.salt), type = "full", by="labsampnum")
      #head(ncss.horizons)
      nrow(ncss.horizons)
      ncss.horizons$oc_d = signif(ncss.horizons$oc / 100 * ncss.horizons$db_od * 1000 * (100 - ifelse(is.na(ncss.horizons$wpg2), 0, ncss.horizons$wpg2))/100, 3)
      ncss.horizons$ca_ext = signif(ncss.horizons$ca_nh4 * 200, 4)
      ncss.horizons$mg_ext = signif(ncss.horizons$mg_nh4 * 121, 3)
      ncss.horizons$na_ext = signif(ncss.horizons$na_nh4 * 230, 3)
      ncss.horizons$k_ext = signif(ncss.horizons$k_nh4 * 391, 3)
      #summary(ncss.horizons$oc_d)
      chemsprops.NCSS = plyr::join(ncss.site[,site.names], ncss.horizons[,hor.names], by="site_key") 
      chemsprops.NCSS$site_obsdate = format(as.Date(chemsprops.NCSS$site_obsdate, format="%m/%d/%Y"), "%Y-%m-%d")
      chemsprops.NCSS$source_db = "USDA_NCSS"
      #dim(chemsprops.NCSS)
      chemsprops.NCSS$oc = chemsprops.NCSS$oc * 10
      chemsprops.NCSS$n_tot = chemsprops.NCSS$n_tot * 10
      #hist(log1p(chemsprops.NCSS$oc), breaks=45, col="gray")
      chemsprops.NCSS$confidence_degree = 1
      chemsprops.NCSS = complete.vars(chemsprops.NCSS, sel=c("tex_psda","oc","clay_tot_psa","ecec","ph_h2o","ec_12pre","k_ext"))
      #rm(ncss.horizons)
    }
    dim(chemsprops.NCSS)

    ## [1] 138535     34

    #summary(!is.na(chemsprops.NCSS$oc))
    ## texture classes need to be cleaned-up
    summary(as.factor(chemsprops.NCSS$tex_psda))

    ##                               c               C              cl 
    ##            2824           11063              19           10278 
    ##              CL             cos             CoS             COS 
    ##               5            2473               6               4 
    ##            cosl            CoSL Fine Sandy Loam              fs 
    ##            4601               2               1            2404 
    ##             fsl             FSL               l               L 
    ##           10907              16           17269               2 
    ##            lcos            LCoS             lfs              ls 
    ##            2995               3            1838            3213 
    ##              LS            lvfs               s               S 
    ##               1             154            3012               1 
    ##              sc             scl             SCL              si 
    ##             609            5552               5             855 
    ##             sic             SiC            sicl            SiCL 
    ##            7218              11           13879              15 
    ##             sil             SiL             SIL              sl 
    ##           22430               4              28            5629 
    ##              SL             vfs            vfsl            VFSL 
    ##              13              67            2711               3 
    ##            NA's 
    ##            6415

#### Africa soil profiles database

-   Leenaars, J. G., Van Oostrum, A. J. M., & Ruiperez Gonzalez, M.
    (2014). [Africa soil profiles database version 1.2. A compilation of
    georeferenced and standardized legacy soil profile data for
    Sub-Saharan Africa (with
    dataset)](https://www.isric.org/projects/africa-soil-profiles-database-afsp).
    Wageningen: ISRIC Report 2014/01; 2014. Data download URL:
    <a href="https://data.isric.org/" class="uri">https://data.isric.org/</a>

<!-- -->

    if(!exists("chemsprops.AfSPDB")){
      library(foreign)
      afspdb.profiles <- read.dbf("/mnt/DATA/Soil_points/AF/AfSIS_SPDB/AfSP012Qry_Profiles.dbf", as.is=TRUE)
      afspdb.layers <- read.dbf("/mnt/DATA/Soil_points/AF/AfSIS_SPDB/AfSP012Qry_Layers.dbf", as.is=TRUE)
      afspdb.s.lst <- c("ProfileID", "FldMnl_ID", "T_Year", "X_LonDD", "Y_LatDD")
      #summary(afspdb.layers$BlkDens)
      ## add missing columns
      for(j in 1:ncol(afspdb.layers)){ 
        if(is.numeric(afspdb.layers[,j])) { 
          afspdb.layers[,j] <- ifelse(afspdb.layers[,j] < 0, NA, afspdb.layers[,j]) 
        }
      }
      afspdb.layers$ca_ext = afspdb.layers$ExCa * 200
      afspdb.layers$mg_ext = afspdb.layers$ExMg * 121
      afspdb.layers$na_ext = afspdb.layers$ExNa * 230
      afspdb.layers$k_ext = afspdb.layers$ExK * 391
      afspdb.m = plyr::join(afspdb.profiles[,afspdb.s.lst], afspdb.layers)
      afspdb.m$oc_d = signif(afspdb.m$OrgC / 1000 * afspdb.m$BlkDens * 1000 * (100 - ifelse(is.na(afspdb.m$CfPc), 0, afspdb.m$CfPc))/100, 3)
      afspdb.h.lst <- c("ProfileID", "FldMnl_ID", "T_Year", "X_LonDD", "Y_LatDD", "LayerID", "LayerNr", "UpDpth", "LowDpth", "HorDes", "LabTxtr", "Clay", "Silt", "Sand", "OrgC", "oc_d", "TotC", "TotalN", "PHKCl", "PHH2O", "PHCaCl2", "CecSoil", "cec_nh4", "Ecec", "CfPc" , "BlkDens", "ca_ext", "mg_ext", "na_ext", "k_ext", "EC", "ec_12pre")
      x.na = afspdb.h.lst[which(!afspdb.h.lst %in% names(afspdb.m))]
      if(length(x.na)>0){ for(i in x.na){ afspdb.m[,i] = NA } }
      chemsprops.AfSPDB = afspdb.m[,afspdb.h.lst]
      chemsprops.AfSPDB$source_db = "AfSPDB"
      chemsprops.AfSPDB$confidence_degree = 5
      chemsprops.AfSPDB = complete.vars(chemsprops.AfSPDB, sel = c("LabTxtr","OrgC","Clay","Ecec","PHH2O","EC","k_ext"), coords = c("X_LonDD", "Y_LatDD"))
    }
    dim(chemsprops.AfSPDB)

    ## [1] 68833    34

#### ISRIC WISE harmonized soil profile data

-   Batjes, N.H. (2019). [Harmonized soil profile data for applications
    at global and continental scales: updates to the WISE
    database](http://dx.doi.org/10.1111/j.1475-2743.2009.00202.x). Soil
    Use and Management 5:124–127. Data download URL:
    <a href="https://files.isric.org/public/wise/WD-WISE.zip" class="uri">https://files.isric.org/public/wise/WD-WISE.zip</a>

<!-- -->

    if(!exists("chemsprops.WISE")){
      wise.site <- read.table("/mnt/DATA/Soil_points/INT/ISRIC_WISE/WISE3_SITE.csv", sep=",", header=TRUE, stringsAsFactors = FALSE, fill=TRUE)
      wise.s.lst <- c("WISE3_id", "PITREF", "DATEYR", "LONDD", "LATDD")
      wise.site$LONDD = as.numeric(wise.site$LONDD)
      wise.site$LATDD = as.numeric(wise.site$LATDD)
      wise.layer <- read.table("/mnt/DATA/Soil_points/INT/ISRIC_WISE/WISE3_HORIZON.csv", sep=",", header=TRUE, stringsAsFactors = FALSE, fill=TRUE)
      wise.layer$ca_ext = signif(wise.layer$EXCA * 200, 4)
      wise.layer$mg_ext = signif(wise.layer$EXMG * 121, 3)
      wise.layer$na_ext = signif(wise.layer$EXNA * 230, 3)
      wise.layer$k_ext = signif(wise.layer$EXK * 391, 3)
      wise.layer$oc_d = signif(wise.layer$ORGC / 1000 * wise.layer$BULKDENS * 1000 * (100 - ifelse(is.na(wise.layer$GRAVEL), 0, wise.layer$GRAVEL))/100, 3)
      wise.h.lst <- c("WISE3_ID", "labsampnum", "HONU", "TOPDEP", "BOTDEP", "DESIG", "tex_psda", "CLAY", "SILT", "SAND", "ORGC", "oc_d", "c_tot", "TOTN", "PHKCL", "PHH2O", "PHCACL2", "CECSOIL", "cec_nh4", "ecec", "GRAVEL" , "BULKDENS", "ca_ext", "mg_ext", "na_ext", "k_ext", "ECE", "ec_12pre")
      x.na = wise.h.lst[which(!wise.h.lst %in% names(wise.layer))]
      if(length(x.na)>0){ for(i in x.na){ wise.layer[,i] = NA } }
      chemsprops.WISE = merge(wise.site[,wise.s.lst], wise.layer[,wise.h.lst], by.x="WISE3_id", by.y="WISE3_ID")
      chemsprops.WISE$source_db = "ISRIC_WISE"
      chemsprops.WISE$confidence_degree = 4
      chemsprops.WISE = complete.vars(chemsprops.WISE, sel = c("ORGC","CLAY","PHH2O","CECSOIL","k_ext"), coords = c("LONDD", "LATDD"))
    }
    dim(chemsprops.WISE)

    ## [1] 23420    34

#### GEMAS

-   Reimann, C., Fabian, K., Birke, M., Filzmoser, P., Demetriades, A.,
    Négrel, P., … & Anderson, M. (2018). [GEMAS: Establishing
    geochemical background and threshold for 53 chemical elements in
    European agricultural
    soil](https://doi.org/10.1016/j.apgeochem.2017.01.021). Applied
    Geochemistry, 88, 302-318. Data download URL:
    <a href="http://gemas.geolba.ac.at/" class="uri">http://gemas.geolba.ac.at/</a>

<!-- -->

    if(!exists("chemsprops.gemas")){
      gemas.samples <- read.csv("/mnt/DATA/Soil_points/EU/GEMAS/GEMAS.csv", stringsAsFactors = FALSE)
      ## GEMAS, agricultural soil, 0-20 cm, air dried, <2 mm, aqua regia Data from ACME, total C, TOC, CEC, ph_CaCl2
      gemas.samples$hzn_top = 0
      gemas.samples$hzn_bot = 20
      gemas.samples$oc = gemas.samples$TOC * 10
      #summary(gemas.samples$oc)
      gemas.samples$c_tot = gemas.samples$C_tot * 10
      gemas.samples$site_obsdate = 2009
      gemas.h.lst <- c("ID", "COUNRTY", "site_obsdate", "XCOO", "YCOO", "labsampnum", "layer_sequence", "hzn_top", "hzn_bot", "TYPE", "tex_psda", "clay", "silt", "sand_tot_psa", "oc", "oc_d", "c_tot", "n_tot", "ph_kcl", "ph_h2o", "pH_CaCl2", "CEC", "cec_nh4", "ecec", "wpg2", "db_od", "ca_ext", "mg_ext", "na_ext", "k_ext", "ec_satp", "ec_12pre")
      x.na = gemas.h.lst[which(!gemas.h.lst %in% names(gemas.samples))]
      if(length(x.na)>0){ for(i in x.na){ gemas.samples[,i] = NA } }
      chemsprops.GEMAS <- gemas.samples[,gemas.h.lst]
      chemsprops.GEMAS$source_db = "GEMAS_2009"
      chemsprops.GEMAS$confidence_degree = 2
      chemsprops.GEMAS = complete.vars(chemsprops.GEMAS, sel = c("oc","clay","pH_CaCl2"), coords = c("XCOO", "YCOO"))
    }
    dim(chemsprops.GEMAS)

    ## [1] 4131   34

#### LUCAS soil

-   Orgiazzi, A., Ballabio, C., Panagos, P., Jones, A., &
    Fernández‐Ugalde, O. (2018). [LUCAS Soil, the largest expandable
    soil dataset for Europe: a
    review](https://doi.org/10.1111/ejss.12499). European Journal of
    Soil Science, 69(1), 140-153. Data download URL:
    <a href="https://esdac.jrc.ec.europa.eu/content/lucas-2009-topsoil-data" class="uri">https://esdac.jrc.ec.europa.eu/content/lucas-2009-topsoil-data</a>

<!-- -->

    if(!exists("chemsprops.LUCAS")){
      lucas.samples <- openxlsx::read.xlsx("/mnt/DATA/Soil_points/EU/LUCAS/LUCAS_TOPSOIL_v1.xlsx", sheet = 1)
      #summary(lucas.samples$N)
      lucas.h.lst <- c("POINT_ID", "usiteid", "site_obsdate", "GPS_LONG", "GPS_LAT", "sample_ID", "layer_sequence", "hzn_top", "hzn_bot", "hzn_desgn", "tex_psda", "clay", "silt", "sand", "OC", "oc_d", "c_tot", "N", "ph_kcl", "pH_in_H2O", "pH_in_CaCl", "CEC", "cec_nh4", "ecec", "coarse", "db_od", "ca_ext", "mg_ext", "na_ext", "K", "ec_satp", "ec_12pre")
      x.na = lucas.h.lst[which(!lucas.h.lst %in% names(lucas.samples))]
      if(length(x.na)>0){ for(i in x.na){ lucas.samples[,i] = NA } }
      chemsprops.LUCAS <- lucas.samples[,lucas.h.lst]
      chemsprops.LUCAS$source_db = "LUCAS_2009"
      chemsprops.LUCAS$hzn_top <- 0
      chemsprops.LUCAS$hzn_bot <- 20
      chemsprops.LUCAS$site_obsdate <- "2009"
      chemsprops.LUCAS$confidence_degree = 2
      chemsprops.LUCAS = complete.vars(chemsprops.LUCAS, sel = c("OC","clay","pH_in_H2O"), coords = c("GPS_LONG", "GPS_LAT"))
    }
    dim(chemsprops.LUCAS)

    ## [1] 19899    34

#### Mangrove forest soil DB

-   Sanderman, J., Hengl, T., Fiske, G., Solvik, K., Adame, M. F.,
    Benson, L., … & Duncan, C. (2018). [A global map of mangrove forest
    soil carbon at 30 m spatial
    resolution](https://doi.org/10.1088/1748-9326/aabe1c). Environmental
    Research Letters, 13(5), 055002. Data download URL:
    <a href="https://dataverse.harvard.edu/dataset.xhtml?persistentId=doi:10.7910/DVN/OCYUIT" class="uri">https://dataverse.harvard.edu/dataset.xhtml?persistentId=doi:10.7910/DVN/OCYUIT</a>

<!-- -->

    if(!exists("chemsprops.Mangroves")){
      mng.profs <- read.csv("/mnt/DATA/Soil_points/INT/TNC_mangroves/mangrove_soc_database_v10_sites.csv", skip=1)
      mng.hors <- read.csv("/mnt/DATA/Soil_points/INT/TNC_mangroves/mangrove_soc_database_v10_horizons.csv", skip=1)
      mngALL = plyr::join(mng.hors, mng.profs, by=c("Site.name"))
      mngALL$oc = mngALL$OC_final * 10
      mngALL$oc_d = mngALL$CD_calc * 1000
      mngALL$hzn_top = mngALL$U_depth * 100
      mngALL$hzn_bot = mngALL$L_depth * 100
      mngALL$wpg2 = 0
      mng.col = c("Site.name", "Site..", "Year_sampled", "Longitude_Adjusted", "Latitude_Adjusted", "labsampnum", "layer_sequence","hzn_top","hzn_bot","hzn_desgn", "tex_psda", "clay_tot_psa", "silt_tot_psa", "sand_tot_psa", "oc", "oc_d", "c_tot", "n_tot", "ph_kcl", "ph_h2o", "ph_cacl2", "cec_sum", "cec_nh4", "ecec", "wpg2", "BD_final", "ca_ext", "mg_ext", "na_ext", "k_ext", "ec_satp", "ec_12pre")
      x.na = mng.col[which(!mng.col %in% names(mngALL))]
      if(length(x.na)>0){ for(i in x.na){ mngALL[,i] = NA } }
      chemsprops.Mangroves = mngALL[,mng.col]
      chemsprops.Mangroves$source_db = "MangrovesDB"
      chemsprops.Mangroves$confidence_degree = 4
      chemsprops.Mangroves = complete.vars(chemsprops.Mangroves, sel = c("oc","BD_final"), coords = c("Longitude_Adjusted", "Latitude_Adjusted"))
      #head(chemsprops.Mangroves)
      #levels(as.factor(mngALL$OK.to.release.))
      mng.rm = chemsprops.Mangroves$Site.name[chemsprops.Mangroves$Site.name %in% mngALL$Site.name[grep("N", mngALL$OK.to.release., ignore.case = FALSE)]]
    }
    dim(chemsprops.Mangroves)

    ## [1] 7987   34

#### CIFOR peatland points

Peatland soil measurements (points) from the literature described in:

-   Murdiyarso, D., Roman-Cuesta, R. M., Verchot, L. V., Herold, M.,
    Gumbricht, T., Herold, N., & Martius, C. (2017). New map reveals
    more peat in the tropics (Vol. 189). CIFOR.

<!-- -->

    if(!exists("chemsprops.Peatlands")){
      cif.hors <- read.csv("/mnt/DATA/Soil_points/INT/CIFOR_peatlands/SOC_literature_CIFOR.csv")
      #summary(cif.hors$BD..g.cm..)
      #summary(cif.hors$SOC)
      cif.hors$oc = cif.hors$SOC * 10
      cif.hors$wpg2 = 0
      cif.hors$c_tot = cif.hors$TOC.content.... * 10
      cif.hors$oc_d = cif.hors$C.density..kg.C.m..
      cif.col = c("SOURCEID", "usiteid", "site_obsdate", "modelling.x", "modelling.y", "labsampnum", "layer_sequence", "Upper", "Lower", "hzn_desgn", "tex_psda", "clay_tot_psa", "silt_tot_psa", "sand_tot_psa", "oc", "oc_d", "c_tot", "n_tot", "ph_kcl", "ph_h2o", "ph_cacl2", "cec_sum", "cec_nh4", "ecec", "wpg2", "BD..g.cm..", "ca_ext", "mg_ext", "na_ext", "k_ext", "ec_satp", "ec_12pre")
      x.na = cif.col[which(!cif.col %in% names(cif.hors))]
      if(length(x.na)>0){ for(i in x.na){ cif.hors[,i] = NA } }
      chemsprops.Peatlands = cif.hors[,cif.col]
      chemsprops.Peatlands$source_db = "CIFOR"
      chemsprops.Peatlands$confidence_degree = 4
      chemsprops.Peatlands = complete.vars(chemsprops.Peatlands, sel = c("oc","BD..g.cm.."), coords = c("modelling.x", "modelling.y"))  
    }
    dim(chemsprops.Peatlands)

    ## [1] 1633   34

#### LandPKS observations

-   Herrick, J. E., Urama, K. C., Karl, J. W., Boos, J., Johnson, M. V.
    V., Shepherd, K. D., … & Kosnik, C. (2013). [The Global
    Land-Potential Knowledge System (LandPKS): Supporting
    Evidence-based, Site-specific Land Use and Management through Cloud
    Computing, Mobile Applications, and
    Crowdsourcing](https://doi.org/10.2489/jswc.68.1.5A). Journal of
    Soil and Water Conservation, 68(1), 5A-12A. Data download URL:
    <a href="http://portal.landpotential.org/#/landpksmap" class="uri">http://portal.landpotential.org/#/landpksmap</a>

<!-- -->

    if(!exists("chemsprops.LandPKS")){
      pks = read.csv("/mnt/DATA/Soil_points/INT/LandPKS/Export_LandInfo_Data.csv", stringsAsFactors = FALSE)
      #str(pks)
      pks.hor = data.frame(rock_fragments = c(pks$rock_fragments_layer_0_1cm,
                                              pks$rock_fragments_layer_1_10cm,
                                              pks$rock_fragments_layer_10_20cm,
                                              pks$rock_fragments_layer_20_50cm,
                                              pks$rock_fragments_layer_50_70cm,
                                              pks$rock_fragments_layer_70_100cm,
                                              pks$rock_fragments_layer_100_120cm), 
                           tex_field = c(pks$texture_layer_0_1cm, 
                                         pks$texture_layer_1_10cm, 
                                         pks$texture_layer_10_20cm, 
                                         pks$texture_layer_20_50cm, 
                                         pks$texture_layer_50_70cm, 
                                         pks$texture_layer_70_100cm, 
                                         pks$texture_layer_100_120cm))
      pks.hor$hzn_top = c(rep(0, nrow(pks)), 
                          rep(1, nrow(pks)), 
                          rep(10, nrow(pks)), 
                          rep(20, nrow(pks)), 
                          rep(50, nrow(pks)), 
                          rep(70, nrow(pks)), 
                          rep(100, nrow(pks)))
      pks.hor$hzn_bot = c(rep(1, nrow(pks)), 
                          rep(10, nrow(pks)), 
                          rep(20, nrow(pks)), 
                          rep(50, nrow(pks)), 
                          rep(70, nrow(pks)), 
                          rep(100, nrow(pks)), 
                          rep(120, nrow(pks)))
      pks.hor$longitude_decimal_degrees = rep(pks$longitude, 7)
      pks.hor$latitude_decimal_degrees = rep(pks$latitude, 7)
      pks.hor$site_obsdate = rep(pks$modified_date, 7)
      pks.hor$site_key = rep(pks$id, 7)
      #summary(as.factor(pks.hor$tex_field))
      tex.tr = data.frame(tex_field=c("CLAY", "CLAY LOAM", "LOAM", "LOAMY SAND", "SAND", "SANDY CLAY", "SANDY CLAY LOAM", "SANDY LOAM", "SILT LOAM", "SILTY CLAY", "SILTY CLAY LOAM"), 
                          clay_tot_psa=c(62.4, 34.0, 19.0, 5.8, 3.3, 41.7, 27.0, 10.0, 13.1, 46.7, 34.0), 
                          silt_tot_psa=c(17.8, 34.0, 40.0, 12.0, 5.0, 6.7, 13.0, 25.0, 65.7, 46.7, 56.0), 
                          sand_tot_psa=c(19.8, 32.0, 41.0, 82.2, 91.7, 51.6, 60.0, 65.0, 21.2, 6.7, 10.0))
      pks.hor$clay_tot_psa = plyr::join(pks.hor["tex_field"], tex.tr)$clay_tot_psa
      pks.hor$silt_tot_psa = plyr::join(pks.hor["tex_field"], tex.tr)$silt_tot_psa
      pks.hor$sand_tot_psa = plyr::join(pks.hor["tex_field"], tex.tr)$sand_tot_psa
      #summary(as.factor(pks.hor$rock_fragments))
      pks.hor$wpg2 = ifelse(pks.hor$rock_fragments==">60%", 65, ifelse(pks.hor$rock_fragments=="35-60%", 47.5, ifelse(pks.hor$rock_fragments=="15-35%", 25, ifelse(pks.hor$rock_fragments=="1-15%" | pks.hor$rock_fragments=="0-15%", 7.5, ifelse(pks.hor$rock_fragments=="0-1%", 0.5, NA)))))
      #head(pks.hor)
      #plot(pks.hor[,c("longitude_decimal_degrees","latitude_decimal_degrees")])
      pks.col = c("site_key", "usiteid", "site_obsdate", "longitude_decimal_degrees", "latitude_decimal_degrees", "labsampnum", "layer_sequence","hzn_top","hzn_bot","hzn_desgn", "tex_field", "clay_tot_psa", "silt_tot_psa", "sand_tot_psa", "oc", "oc_d", "c_tot", "n_tot", "ph_kcl", "ph_h2o", "ph_cacl2", "cec_sum", "cec_nh4", "ecec", "wpg2", "db_od", "ca_ext", "mg_ext", "na_ext", "k_ext", "ec_satp", "ec_12pre")
      x.na = pks.col[which(!pks.col %in% names(pks.hor))]
      if(length(x.na)>0){ for(i in x.na){ pks.hor[,i] = NA } }
      chemsprops.LandPKS = pks.hor[,pks.col]
      chemsprops.LandPKS$source_db = "LandPKS"
      chemsprops.LandPKS$confidence_degree = 8
      chemsprops.LandPKS = complete.vars(chemsprops.LandPKS, sel = c("clay_tot_psa","wpg2"), coords = c("longitude_decimal_degrees", "latitude_decimal_degrees"))
    }
    dim(chemsprops.LandPKS)

    ## [1] 69548    34

#### EGRPR

-   [Russian Federation: The Unified State Register of Soil Resources
    (EGRPR)](http://egrpr.esoil.ru/). Data download URL:
    <a href="http://egrpr.esoil.ru/content/1DB.html" class="uri">http://egrpr.esoil.ru/content/1DB.html</a>

<!-- -->

    if(!exists("chemsprops.EGRPR")){
      russ.HOR = read.csv("/mnt/DATA/Soil_points/Russia/EGRPR/Russia_EGRPR_soil_pedons.csv")
      russ.HOR$SOURCEID = paste(russ.HOR$CardID, russ.HOR$SOIL_ID, sep="_")
      russ.HOR$wpg2 = russ.HOR$TEXTSTNS
      russ.HOR$SNDPPT <- russ.HOR$TEXTSAF + russ.HOR$TEXSCM
      russ.HOR$SLTPPT <- russ.HOR$TEXTSIC + russ.HOR$TEXTSIM + 0.8 * russ.HOR$TEXTSIF
      russ.HOR$CLYPPT <- russ.HOR$TEXTCL + 0.2 * russ.HOR$TEXTSIF
      ## Correct texture fractions:
      sumTex <- rowSums(russ.HOR[,c("SLTPPT","CLYPPT","SNDPPT")])
      russ.HOR$SNDPPT <- russ.HOR$SNDPPT / ((sumTex - russ.HOR$CLYPPT) /(100 - russ.HOR$CLYPPT))
      russ.HOR$SLTPPT <- russ.HOR$SLTPPT / ((sumTex - russ.HOR$CLYPPT) /(100 - russ.HOR$CLYPPT))
      russ.HOR$oc <- rowMeans(data.frame(x1=russ.HOR$CORG * 10, x2=russ.HOR$ORGMAT/1.724 * 10), na.rm=TRUE)
      russ.HOR$oc_d = signif(russ.HOR$oc / 1000 * russ.HOR$DVOL * 1000 * (100 - ifelse(is.na(russ.HOR$wpg2), 0, russ.HOR$wpg2))/100, 3)
      russ.HOR$n_tot <- russ.HOR$NTOT * 10
      russ.HOR$ca_ext = russ.HOR$EXCA * 200
      russ.HOR$mg_ext = russ.HOR$EXMG * 121
      russ.HOR$na_ext = russ.HOR$EXNA * 230
      russ.HOR$k_ext = russ.HOR$EXK * 391
      russ.sel.h <- c("SOURCEID", "SOIL_ID", "site_obsdate", "LONG", "LAT", "labsampnum", "HORNMB", "HORTOP", "HORBOT", "HISMMN", "tex_psda", "CLYPPT", "SLTPPT", "SNDPPT", "oc", "oc_d", "c_tot", "NTOT", "PHSLT", "PHH2O", "ph_cacl2", "CECST", "cec_nh4", "ecec", "wpg2", "DVOL", "ca_ext", "mg_ext", "na_ext", "k_ext", "ec_satp", "ec_12pre")
      x.na = russ.sel.h[which(!russ.sel.h %in% names(russ.HOR))]
      if(length(x.na)>0){ for(i in x.na){ russ.HOR[,i] = NA } }
      chemsprops.EGRPR = russ.HOR[,russ.sel.h]
      chemsprops.EGRPR$source_db = "Russia_EGRPR"
      chemsprops.EGRPR$confidence_degree = 2
      chemsprops.EGRPR <- complete.vars(chemsprops.EGRPR, sel=c("oc", "CLYPPT"), coords = c("LONG", "LAT"))
    }
    dim(chemsprops.EGRPR)

    ## [1] 4328   34

#### Canada National Pedon DB

-   [Agriculture and Agri-Food Canada National Pedon
    Database](https://open.canada.ca/data/en/dataset/6457fad6-b6f5-47a3-9bd1-ad14aea4b9e0).
    Data download URL:
    <a href="https://open.canada.ca/data/en/" class="uri">https://open.canada.ca/data/en/</a>

<!-- -->

    if(!exists("chemsprops.NPDB")){
      NPDB.nm = c("NPDB_V2_sum_source_info.csv","NPDB_V2_sum_chemical.csv", "NPDB_V2_sum_horizons_raw.csv", "NPDB_V2_sum_physical.csv")
      NPDB.HOR = plyr::join_all(lapply(paste0("/mnt/DATA/Soil_points/Canada/NPDB/", NPDB.nm), read.csv), type = "full")
      NPDB.HOR$HISMMN = paste0(NPDB.HOR$HZN_MAS, NPDB.HOR$HZN_SUF, NPDB.HOR$HZN_MOD)
      NPDB.HOR$CARB_ORG[NPDB.HOR$CARB_ORG==9] <- NA
      NPDB.HOR$N_TOTAL[NPDB.HOR$N_TOTAL==9] <- NA
      NPDB.HOR$oc = NPDB.HOR$CARB_ORG * 10
      NPDB.HOR$oc_d = signif(NPDB.HOR$oc / 1000 * NPDB.HOR$BULK_DEN * 1000 * (100 - ifelse(is.na(NPDB.HOR$VC_SAND), 0, NPDB.HOR$VC_SAND))/100, 3)
      NPDB.HOR$ca_ext = NPDB.HOR$EXCH_CA * 200
      NPDB.HOR$mg_ext = NPDB.HOR$EXCH_MG * 121
      NPDB.HOR$na_ext = NPDB.HOR$EXCH_NA * 230
      NPDB.HOR$k_ext = NPDB.HOR$EXCH_K * 391
      npdb.sel.h = c("PEDON_ID", "usiteid", "CAL_YEAR", "DD_LONG", "DD_LAT", "labsampnum", "layer_sequence", "U_DEPTH", "L_DEPTH", "HISMMN", "tex_psda", "T_CLAY", "T_SILT", "T_SAND", "oc", "oc_d", "c_tot", "N_TOTAL", "ph_kcl", "PH_H2O", "PH_CACL2", "CEC", "cec_nh4", "ecec", "VC_SAND", "BULK_DEN", "ca_ext", "mg_ext", "na_ext", "k_ext", "ec_satp", "ec_12pre")
      x.na = npdb.sel.h[which(!npdb.sel.h %in% names(NPDB.HOR))]
      if(length(x.na)>0){ for(i in x.na){ NPDB.HOR[,i] = NA } }
      chemsprops.NPDB = NPDB.HOR[,npdb.sel.h]
      chemsprops.NPDB$source_db = "Canada_NPDB"
      chemsprops.NPDB$confidence_degree = 2
      chemsprops.NPDB <- complete.vars(chemsprops.NPDB, sel=c("oc", "PH_H2O", "T_CLAY"), coords = c("DD_LONG", "DD_LAT"))
    }
    dim(chemsprops.NPDB)

    ## [1] 16405    34

#### Canadian upland forest soil profile and carbon stocks database

-   Shaw, C., Hilger, A., Filiatrault, M., & Kurz, W. (2018). [A
    Canadian upland forest soil profile and carbon stocks
    database](https://doi.org/10.1002/ecy.2159). Ecology, 99(4),
    989-989. Data download URL:
    <a href="https://esajournals.onlinelibrary.wiley.com/action/downloadSupplement?doi=10.1002%2Fecy.2159&amp;file=ecy2159-sup-0001-DataS1.zip" class="uri">https://esajournals.onlinelibrary.wiley.com/action/downloadSupplement?doi=10.1002%2Fecy.2159&amp;file=ecy2159-sup-0001-DataS1.zip</a>

\*Organic horizons have negative values, the first mineral soil horizon
has a value of 0 cm, and other mineral soil horizons have positive
values. This needs to be corrected before the values can be bind with
other international sets.

    if(!exists("chemsprops.CUFS")){
      ## Reading of the .dat file was tricky 
      cufs.HOR = read.csv("/mnt/DATA/Soil_points/Canada/CUFSDB/PROFILES.csv", stringsAsFactors = FALSE)
      cufs.HOR$LOWER_HZN_LIMIT =cufs.HOR$UPPER_HZN_LIMIT + cufs.HOR$HZN_THICKNESS
      ## Correct depth (Canadian data can have negative depths):
      z.min.cufs <- ddply(cufs.HOR, .(LOCATION_ID), summarize, aggregated = min(UPPER_HZN_LIMIT, na.rm=TRUE))
      z.shift.cufs <- join(cufs.HOR["LOCATION_ID"], z.min.cufs, type="left")$aggregated
      ## fixed shift
      z.shift.cufs <- ifelse(z.shift.cufs>0, 0, z.shift.cufs)
      cufs.HOR$hzn_top <- cufs.HOR$UPPER_HZN_LIMIT - z.shift.cufs
      cufs.HOR$hzn_bot <- cufs.HOR$LOWER_HZN_LIMIT - z.shift.cufs
      cufs.SITE = read.csv("/mnt/DATA/Soil_points/Canada/CUFSDB/SITES.csv", stringsAsFactors = FALSE)
      cufs.HOR$longitude_decimal_degrees = plyr::join(cufs.HOR["LOCATION_ID"], cufs.SITE)$LONGITUDE
      cufs.HOR$latitude_decimal_degrees = plyr::join(cufs.HOR["LOCATION_ID"], cufs.SITE)$LATITUDE
      cufs.HOR$site_obsdate = plyr::join(cufs.HOR["LOCATION_ID"], cufs.SITE)$YEAR_SAMPLED
      cufs.HOR$usiteid = plyr::join(cufs.HOR["LOCATION_ID"], cufs.SITE)$RELEASE_SOURCE_SITEID
      #summary(cufs.HOR$ORG_CARB_PCT)
      cufs.HOR$oc = cufs.HOR$ORG_CARB_PCT*10
      cufs.HOR$c_tot = cufs.HOR$oc + ifelse(is.na(cufs.HOR$CARBONATE_CARB_PCT), 0, cufs.HOR$CARBONATE_CARB_PCT*10)
      cufs.HOR$n_tot = cufs.HOR$TOT_NITRO_PCT*10
      cufs.HOR$ca_ext = cufs.HOR$EXCH_Ca * 200
      cufs.HOR$mg_ext = cufs.HOR$EXCH_Mg * 121
      cufs.HOR$na_ext = cufs.HOR$EXCH_Na * 230
      cufs.HOR$k_ext = cufs.HOR$EXCH_K * 391
      cufs.HOR$ph_cacl2 = cufs.HOR$pH
      cufs.HOR$ph_cacl2[!cufs.HOR$pH_H2O_CACL2=="CACL2"] = NA
      cufs.HOR$ph_h2o = cufs.HOR$pH
      cufs.HOR$ph_h2o[!cufs.HOR$pH_H2O_CACL2=="H2O"] = NA
      #summary(cufs.HOR$CF_VOL_PCT) ## is NA == 0??
      cufs.HOR$wpg2 = ifelse(cufs.HOR$CF_CORR_FACTOR==1, 0, cufs.HOR$CF_VOL_PCT)
      cufs.HOR$oc_d = signif(cufs.HOR$oc / 1000 * cufs.HOR$BULK_DENSITY * 1000 * (100 - ifelse(is.na(cufs.HOR$wpg2), 0, cufs.HOR$wpg2))/100, 3)
      cufs.sel.h = c("LOCATION_ID", "usiteid", "site_obsdate", "longitude_decimal_degrees", "latitude_decimal_degrees", "labsampnum", "HZN_SEQ_NO", "hzn_top", "hzn_bot", "HORIZON", "TEXT_CLASS", "CLAY_PCT", "SILT_PCT", "SAND_PCT", "oc", "oc_d", "c_tot", "n_tot", "ph_kcl", "ph_h2o", "ph_cacl2", "CEC_CALCULATED", "cec_nh4", "ecec", "wpg2", "BULK_DENSITY", "ca_ext", "mg_ext", "na_ext", "k_ext", "ELEC_COND", "ec_12pre")
      x.na = cufs.sel.h[which(!cufs.sel.h %in% names(cufs.HOR))]
      if(length(x.na)>0){ for(i in x.na){ cufs.HOR[,i] = NA } }
      chemsprops.CUFS = cufs.HOR[,cufs.sel.h]
      chemsprops.CUFS$source_db = "Canada_CUFS"
      chemsprops.CUFS$confidence_degree = 1
      chemsprops.CUFS <- complete.vars(chemsprops.CUFS, sel=c("oc", "ph_h2o", "CLAY_PCT"))
    }
    dim(chemsprops.CUFS)

    ## [1] 15873    34

#### SOTER China soil profiles

-   Dijkshoorn, K., van Engelen, V., & Huting, J. (2008). [Soil and
    landform properties for LADA partner
    countries](https://isric.org/sites/default/files/isric_report_2008_06.pdf).
    ISRIC report 2008/06 and GLADA report 2008/03, ISRIC – World Soil
    Information and FAO, Wageningen. Data download URL:
    <a href="https://files.isric.org/public/soter/CN-SOTER.zip" class="uri">https://files.isric.org/public/soter/CN-SOTER.zip</a>

<!-- -->

    if(!exists("chemsprops.CNSOTER")){
      sot.sites = read.csv("/mnt/DATA/Soil_points/China/China_SOTERv1/CHINA_SOTERv1_Profile.csv")
      sot.horizons = read.csv("/mnt/DATA/Soil_points/China/China_SOTERv1/CHINA_SOTERv1_Horizon.csv")
      sot.HOR = plyr::join_all(list(sot.sites, sot.horizons), type = "full")
      sot.HOR$oc = sot.HOR$SOCA * 10
      sot.HOR$ca_ext = sot.HOR$EXCA * 200
      sot.HOR$mg_ext = sot.HOR$EXMG * 121
      sot.HOR$na_ext = sot.HOR$EXNA * 230
      sot.HOR$k_ext = sot.HOR$EXCK * 391
      ## upper depth missing needs to be derived manually
      sot.HOR$hzn_top = NA
      sot.HOR$hzn_top[2:nrow(sot.HOR)] <- sot.HOR$HBDE[1:(nrow(sot.HOR)-1)]
      sot.HOR$hzn_top <- ifelse(sot.HOR$hzn_top > sot.HOR$HBDE, 0, sot.HOR$hzn_top)
      sot.HOR$hzn_top <- ifelse(sot.HOR$HONU==1 & is.na(sot.HOR$hzn_top), 0, sot.HOR$hzn_top)
      sot.HOR$oc_d = signif(sot.HOR$oc / 1000 * sot.HOR$BULK * 1000 * (100 - ifelse(is.na(sot.HOR$SDVC), 0, sot.HOR$SDVC))/100, 3)
      sot.sel.h = c("PRID", "PDID", "SAYR", "LNGI", "LATI", "labsampnum", "HONU", "hzn_top","HBDE","HODE", "PSCL", "CLPC", "STPC", "SDTO", "oc", "oc_d", "TOTC", "TOTN", "PHKC", "PHAQ", "ph_cacl2", "CECS", "cec_nh4", "ecec", "SDVC", "BULK", "ca_ext", "mg_ext", "na_ext", "k_ext", "ec_satp", "ec_12pre")
      x.na = sot.sel.h[which(!sot.sel.h %in% names(sot.HOR))]
      if(length(x.na)>0){ for(i in x.na){ sot.HOR[,i] = NA } }
      chemsprops.CNSOT = sot.HOR[,sot.sel.h]
      chemsprops.CNSOT$source_db = "China_SOTER"
      chemsprops.CNSOT$confidence_degree = 8
      chemsprops.CNSOT <- complete.vars(chemsprops.CNSOT, sel=c("TOTC", "PHAQ", "CLPC"), coords = c("LNGI", "LATI"))
    }

    ## Joining by: PRID, INFR

    dim(chemsprops.CNSOT)

    ## [1] 5115   34

#### SISLAC

-   Sistema de Información de Suelos de Latinoamérica (SISLAC), Data
    download URL:
    <a href="http://54.229.242.119/sislac/es" class="uri">http://54.229.242.119/sislac/es</a>

<!-- -->

    if(!exists("chemsprops.SISLAC")){
      sis.hor = read.csv("/mnt/DATA/Soil_points/SA/SISLAC/sislac_profiles_es.csv", stringsAsFactors = FALSE)
      #str(sis.hor)
      ## SOC for Urugvay do not match the original soil profile data (see e.g. http://www.mgap.gub.uy/sites/default/files/multimedia/skmbt_c45111090914030.pdf)
      ## compare with:
      #sis.hor[sis.hor$perfil_id=="23861",]
      ## Subset to SISINTA/WOSIS points:
      cor.sel = c(grep("WoSIS", paste(sis.hor$perfil_numero)), grep("SISINTA", paste(sis.hor$perfil_numero)))
      #length(cor.sel)
      sis.hor = sis.hor[cor.sel,]
      #summary(sis.hor$analitico_carbono_organico_c)
      sis.hor$oc = sis.hor$analitico_carbono_organico_c * 10
      sis.hor$oc_d = signif(sis.hor$oc / 1000 * sis.hor$analitico_densidad_aparente * 1000 * (100 - ifelse(is.na(sis.hor$analitico_gravas), 0, sis.hor$analitico_gravas))/100, 3)
      #summary(sis.hor$analitico_base_k)
      sis.sel.h = c("perfil_id", "perfil_numero", "perfil_fecha", "perfil_ubicacion_longitud", "perfil_ubicacion_latitud", "id", "layer_sequence", "profundidad_superior", "profundidad_inferior", "hzn_desgn", "tex_psda", "analitico_arcilla", "analitico_limo_2_50", "analitico_arena_total", "oc", "oc_d", "c_tot", "n_tot", "analitico_ph_kcl", "analitico_ph_h2o", "ph_cacl2", "cec_sum", "cec_nh4", "ecec", "analitico_gravas", "analitico_densidad_aparente", "ca_ext", "mg_ext", "na_ext", "k_ext", "analitico_conductividad", "ec_12pre")
      x.na = sis.sel.h[which(!sis.sel.h %in% names(sis.hor))]
      if(length(x.na)>0){ for(i in x.na){ sis.hor[,i] = NA } }
      chemsprops.SISLAC = sis.hor[,sis.sel.h]
      chemsprops.SISLAC$source_db = "SISLAC"
      chemsprops.SISLAC$confidence_degree = 4
      chemsprops.SISLAC <- complete.vars(chemsprops.SISLAC, sel=c("oc","analitico_ph_kcl","analitico_arcilla"), coords = c("perfil_ubicacion_longitud", "perfil_ubicacion_latitud"))
    }
    dim(chemsprops.SISLAC)

    ## [1] 54732    34

#### FEBR

-   Free Brazilian Repository for Open Soil Data – febr. Data download
    URL:
    <a href="http://www.ufsm.br/febr/" class="uri">http://www.ufsm.br/febr/</a>

<!-- -->

    if(!exists("chemsprops.FEBR")){
      #library(febr)
      ## download up-to-date copy of data
      #febr.lab = febr::layer(dataset = "all", variable="all")
      #febr.lab = febr::observation(dataset = "all")
      febr.hor = read.csv("/mnt/DATA/Soil_points/Brasil/FEBR/febr-superconjunto.csv", stringsAsFactors = FALSE, dec = ",", sep = ";")
      #head(febr.hor)
      #summary(febr.hor$carbono)
      #summary(febr.hor$ph)
      #summary(febr.hor$dsi) ## bulk density of total soil
      febr.hor$clay_tot_psa = febr.hor$argila /10
      febr.hor$sand_tot_psa = febr.hor$areia /10
      febr.hor$silt_tot_psa = febr.hor$silte /10
      febr.hor$wpg2 = (1000-febr.hor$terrafina)/10
      febr.hor$oc_d = signif(febr.hor$carbono / 1000 * febr.hor$dsi * 1000 * (100 - ifelse(is.na(febr.hor$wpg2), 0, febr.hor$wpg2))/100, 3)
      febr.sel.h <- c("observacao_id", "usiteid", "observacao_data", "coord_x", "coord_y", "sisb_id", "camada_id", "profund_sup", "profund_inf", "camada_nome", "tex_psda", "clay_tot_psa", "silt_tot_psa", "sand_tot_psa", "carbono", "oc_d", "c_tot", "nitrogenio", "ph_kcl", "ph", "ph_cacl2", "cec_sum", "cec_nh4", "ecec", "wpg2", "dsi", "ca_ext", "mg_ext", "na_ext", "k_ext", "ce", "ec_12pre")
      x.na = febr.sel.h[which(!febr.sel.h %in% names(febr.hor))]
      if(length(x.na)>0){ for(i in x.na){ febr.hor[,i] = NA } }
      chemsprops.FEBR = febr.hor[,febr.sel.h]
      chemsprops.FEBR$source_db = "FEBR"
      chemsprops.FEBR$confidence_degree = 4
      chemsprops.FEBR <- complete.vars(chemsprops.FEBR, sel=c("carbono","ph","clay_tot_psa","dsi"), coords = c("coord_x", "coord_y"))
    }
    dim(chemsprops.FEBR)

    ## [1] 8001   34

#### Soil Profile DB for Costa Rica

-   Mata, R., Vázquez, A., Rosales, A., & Salazar, D. (2012). [Mapa
    digital de suelos de Costa
    Rica](http://www.cia.ucr.ac.cr/?page_id=139). Asociación
    Costarricense de la Ciencia del Suelo, San José, CRC. Escala,
    1, 200000. Data download URL:
    <a href="http://www.cia.ucr.ac.cr/wp-content/recursosnaturales/Base%20perfiles%20de%20suelos%20v1.1.rar" class="uri">http://www.cia.ucr.ac.cr/wp-content/recursosnaturales/Base%20perfiles%20de%20suelos%20v1.1.rar</a>

<!-- -->

    if(!exists("chemsprops.CostaRica")){
      cr.hor = read.csv("/mnt/DATA/Soil_points/Costa_Rica/Base_de_datos_version_1.2.3.csv", stringsAsFactors = FALSE)
      #plot(cr.hor[,c("X","Y")], pch="+", asp=1)
      cr.hor$usiteid = paste(cr.hor$Provincia, cr.hor$Cantón, cr.hor$Id, sep="_")
      #summary(cr.hor$Corg.)
      cr.hor$oc = cr.hor$Corg. * 10
      cr.hor$Densidad.Aparente = as.numeric(paste0(cr.hor$Densidad.Aparente))
      #summary(cr.hor$K)
      cr.hor$ca_ext = cr.hor$Ca * 200
      cr.hor$mg_ext = cr.hor$Mg * 121
      #cr.hor$na_ext = cr.hor$Na * 230
      cr.hor$k_ext = cr.hor$K * 391
      cr.hor$wpg2 = NA
      cr.hor$oc_d = signif(cr.hor$oc / 1000 * cr.hor$Densidad.Aparente * 1000 * (100 - ifelse(is.na(cr.hor$wpg2), 0, cr.hor$wpg2))/100, 3)
      cr.sel.h = c("Id", "usiteid", "Fecha", "X", "Y", "labsampnum", "horizonte", "prof_inicio", "prof_final", "id_hz", "Clase.Textural", "ARCILLA", "LIMO", "ARENA", "oc", "oc_d", "c_tot", "n_tot", "pHKCl", "pH_H2O", "ph_cacl2", "cec_sum", "cec_nh4", "ecec", "wpg2", "Densidad.Aparente", "ca_ext", "mg_ext", "na_ext", "k_ext", "ec_satp", "ec_12pre")
      x.na = cr.sel.h[which(!cr.sel.h %in% names(cr.hor))]
      if(length(x.na)>0){ for(i in x.na){ cr.hor[,i] = NA } }
      chemsprops.CostaRica = cr.hor[,cr.sel.h]
      chemsprops.CostaRica$source_db = "CostaRica"
      chemsprops.CostaRica$confidence_degree = 4
      chemsprops.CostaRica <- complete.vars(chemsprops.CostaRica, sel=c("oc","pH_H2O","ARCILLA","Densidad.Aparente"), coords = c("X", "Y"))
    }
    dim(chemsprops.CostaRica)

    ## [1] 2051   34

#### Iran soil profile DB

-   Dewan, M. L., & Famouri, J. (1964). The soils of Iran. Food and
    Agriculture Organization of the United Nations.
-   Hengl, T., Toomanian, N., Reuter, H. I., & Malakouti, M. J. (2007).
    [Methods to interpolate soil categorical variables from profile
    observations: Lessons from
    Iran](https://doi.org/10.1016/j.geoderma.2007.04.022). Geoderma,
    140(4), 417-427.
-   Mohammad, H. B. (2000). Soil resources and use potentiality map of
    Iran. Soil and Water Research Institute, Teheran, Iran.

<!-- -->

    if(!exists("chemsprops.IRANSPDB")){
      na.s = c("?","","?.","??", -2147483647, -1.00e+308, "<NA>")
      iran.hor = read.csv("/mnt/DATA/Soil_points/Iran/iran_sdbana.txt", stringsAsFactors = FALSE, na.strings = na.s, header = FALSE)[,1:12]
      names(iran.hor) = c("site_key", "hzn_desgn", "hzn_top", "hzn_bot", "ph_h2o", "ec_satp", "oc", "CACO", "PBS", "sand_tot_psa", "silt_tot_psa", "clay_tot_psa")
      iran.hor$hzn_top = ifelse(is.na(iran.hor$hzn_top) & iran.hor$hzn_desgn=="A", 0, iran.hor$hzn_top)
      iran.hor2 = read.csv("/mnt/DATA/Soil_points/Iran/iran_sdbhor.txt", stringsAsFactors = FALSE, na.strings = na.s, header = FALSE)[,1:8]
      names(iran.hor2) = c("site_key", "layer_sequence", "DESI", "hzn_top", "hzn_bot", "M_colour", "tex_psda", "hzn_desgn")
      iran.site = read.csv("/mnt/DATA/Soil_points/Iran/iran_sgdb.txt", stringsAsFactors = FALSE, na.strings = na.s, header = FALSE)
      names(iran.site) = c("usiteid", "latitude_decimal_degrees", "longitude_decimal_degrees", "FAO", "Tax", "site_key")
      iran.db = plyr::join_all(list(iran.site, iran.hor, iran.hor2))
      iran.db$oc = iran.db$oc * 10
      #summary(iran.db$oc)
      x.na = col.names[which(!col.names %in% names(iran.db))]
      if(length(x.na)>0){ for(i in x.na){ iran.db[,i] = NA } }
      chemsprops.IRANSPDB = iran.db[,col.names]
      chemsprops.IRANSPDB$source_db = "Iran_SPDB"
      chemsprops.IRANSPDB$confidence_degree = 4
      chemsprops.IRANSPDB <- complete.vars(chemsprops.IRANSPDB, sel=c("oc","ph_h2o","clay_tot_psa"))
    }
    dim(chemsprops.IRANSPDB)

    ## [1] 4942   34

#### Northern circumpolar permafrost soil profiles

-   Hugelius, G., Bockheim, J. G., Camill, P., Elberling, B., Grosse,
    G., Harden, J. W., … & Michaelson, G. (2013). [A new data set for
    estimating organic carbon storage to 3 m depth in soils of the
    northern circumpolar permafrost
    region](https://doi.org/10.5194/essd-5-393-2013). Earth System
    Science Data (Online), 5(2). Data download URL:
    <a href="http://dx.doi.org/10.5879/ECDS/00000002" class="uri">http://dx.doi.org/10.5879/ECDS/00000002</a>

<!-- -->

    if(!exists("chemsprops.NCSCD")){
      ncscd.hors <- read.csv("/mnt/DATA/Soil_points/INT/NCSCD/Harden_etal_2012_Hugelius_etal_2013_cleaned_data.csv", stringsAsFactors = FALSE)
      ncscd.hors$oc = as.numeric(ncscd.hors$X.C)*10
      #summary(ncscd.hors$oc)
      #hist(ncscd.hors$Layer.thickness.cm, breaks = 45)
      ncscd.hors$Layer.thickness.cm = ifelse(ncscd.hors$Layer.thickness.cm<0, NA, ncscd.hors$Layer.thickness.cm)
      ncscd.hors$hzn_bot = ncscd.hors$Basal.Depth.cm + ncscd.hors$Layer.thickness.cm
      ncscd.hors$db_od = as.numeric(ncscd.hors$bulk.density.g.cm.3)
      ## Can we assume no coarse fragments?
      ncscd.hors$wpg2 = 0
      ncscd.hors$oc_d = signif(ncscd.hors$oc / 1000 * ncscd.hors$db_od * 1000 * (100 - ifelse(is.na(ncscd.hors$wpg2), 0, ncscd.hors$wpg2))/100, 3)
      ## very high values >40 kg/m3
      ncscd.hors$site_obsdate = format(as.Date(ncscd.hors$Sample.date, format="%d-%m-%Y"), "%Y-%m-%d")
      #summary(ncscd.hors$db_od)
      ncscd.col = c("Profile.ID", "citation", "site_obsdate", "Long", "Lat", "labsampnum", "layer_sequence", "Basal.Depth.cm", "hzn_bot", "Horizon.type", "tex_psda", "clay_tot_psa", "silt_tot_psa", "sand_tot_psa", "oc", "oc_d", "c_tot", "n_tot", "ph_kcl", "ph_h2o", "ph_cacl2", "cec_sum", "cec_nh4", "ecec", "wpg2", "db_od", "ca_ext", "mg_ext", "na_ext", "k_ext", "ec_satp", "ec_12pre")
      x.na = ncscd.col[which(!ncscd.col %in% names(ncscd.hors))]
      if(length(x.na)>0){ for(i in x.na){ ncscd.hors[,i] = NA } }
      chemsprops.NCSCD = ncscd.hors[,ncscd.col]
      chemsprops.NCSCD$source_db = "NCSCD"
      chemsprops.NCSCD$confidence_degree = 10
      chemsprops.NCSCD = complete.vars(chemsprops.NCSCD, sel = c("oc","db_od"), coords = c("Long", "Lat"))  
    }
    dim(chemsprops.NCSCD)

    ## [1] 7371   34

#### CSIRO National Soil Site Database

-   CSIRO (2020). CSIRO National Soil Site Database. v4. CSIRO. Data
    Collection.
    <a href="https://data.csiro.au/collections/#/collection/CI40400" class="uri">https://data.csiro.au/collections/#/collection/CI40400</a>.
    Data download URL:
    <a href="https://doi.org/10.25919/5eeb2a56eac12" class="uri">https://doi.org/10.25919/5eeb2a56eac12</a>
    (available upon request)

<!-- -->

    if(!exists("chemsprops.NatSoil")){
      library(Hmisc) 
      cmdb <- mdb.get("/mnt/DATA/Soil_points/Australia/CSIRO/NatSoil_v2_20200612.mdb")
      #str(cmdb$SITES)
      au.obs = cmdb$OBSERVATIONS[,c("s.id", "o.location.notes", "o.date.desc", "o.latitude.GDA94", "o.longitude.GDA94")]
      au.obs = au.obs[!is.na(au.obs$o.longitude.GDA94),]
      coordinates(au.obs) <- ~o.longitude.GDA94+o.latitude.GDA94
      proj4string(au.obs) <- CRS("+proj=longlat +ellps=GRS80 +no_defs")
      au.xy <- data.frame(spTransform(au.obs, CRS("+proj=longlat +ellps=WGS84 +datum=WGS84")))
      #plot(au.xy[,c("o.longitude.GDA94", "o.latitude.GDA94")])
      ## all variables in one column and need to be sorted based on the lab method
      #summary(cmdb$LAB_METHODS$LABM.SHORT.NAME)
      #write.csv(cmdb$LAB_METHODS, "/mnt/DATA/Soil_points/Australia/CSIRO/NatSoil_v2_20200612_lab_methods.csv")
      lab.tbl = list(
              c("6_DC", "6A1", "6A1_UC", "6B1", "6B2", "6B2a", "6B2b", "6B3", "6B4", "6B4a", "6B4b", "6Z"), # %
              c("6B3a"), # g/kg
              c("6H4", "6H4_SCaRP"), # %
              c("7_C_B", "7_NR", "7A1", "7A2", "7A2a", "7A2b", "7A3", "7A4", "7A5", "7A6", "7A6a", "7A6b", "7A6b_MCLW"),  # g/kg
              c("4A1", "4_NR", "4A_C_2.5", "4A_C_1", "4G1"),
              c("4C_C_1", "4C1", "4C2", "23A"),
              c("4B_C_2.5", "4B1", "4B2"),
              c("P10_NR_C", "P10_HYD_C", "P10_PB_C", "P10_PB1_C", "P10_CF_C", "P10_I_C"),
              c("P10_NR_Z", "P10_HYD_Z", "P10_PB_Z", "P10_PB1_Z", "P10_CF_Z", "P10_I_Z"),
              c("P10_NR_S", "P10_HYD_S", "P10_PB_S", "P10_PB1_S", "P10_CF_S", "P10_I_S"),
              c("15C1modCEC", "15_HSK_CEC", "15J_CEC"),
              c("15I1", "15I2", "15I3", "15I4", "15D3_CEC"),
              c("15_BASES", "15_NR", "15J_H", "15J1"),
              c("2Z2_Grav", "P10_GRAV"),
              c("503.08a", "P3A_NR", "P3A1", "P3A1_C4", "P3A1_CLOD", "P3A1_e"),
              c("18F1_CA"),
              c("18F1_MG"),
              c("18F1_NA"),
              c("18F1_K", "18F2", "18A1mod", "18_NR", "18A1", "18A1_NR", "18B1", "18B2"),
              c("3_C_B", "3_NR", "3A_TSS"),
              c("3A_C_2.5", "3A1")
              )
      names(lab.tbl) = c("oc", "ocP", "c_tot", "n_tot", "ph_h2o", "ph_kcl", "ph_cacl2", "clay_tot_psa", "silt_tot_psa", "sand_tot_psa", "cec_sum", "cec_nh4", "ecec", "wpg2", "db_od", "ca_ext", "mg_ext", "na_ext", "k_ext", "ec_satp", "ec_12pre")
      val.lst = lapply(1:length(lab.tbl), function(i){x <- cmdb$LAB_RESULTS[cmdb$LAB_RESULTS$labm.code %in% lab.tbl[[i]], c("agency.code", "proj.code", "s.id", "o.id", "h.no", "labr.value")]; names(x)[6] <- names(lab.tbl)[i]; return(x) })
      names(val.lst) = names(lab.tbl)
      val.lst$oc$oc = val.lst$oc$oc * 10
      names(val.lst$ocP)[6] = "oc"
      val.lst$oc <- rbind(val.lst$oc, val.lst$ocP)
      val.lst$ocP = NULL
      #summary(val.lst$oc$oc)
      #str(val.lst, max.level = 1)
      for(i in 1:length(val.lst)){ val.lst[[i]]$h.id <- paste(val.lst[[i]]$agency.code, val.lst[[i]]$proj.code, val.lst[[i]]$s.id, val.lst[[i]]$o.id, val.lst[[i]]$h.no, sep="_") }
      au.hor <- plyr::join_all(lapply(val.lst, function(x){x[,6:7]}), match="first")
      #str(as.factor(au.hor$h.id))
      cmdb$HORIZONS$h.id = paste(cmdb$HORIZONS$agency.code, cmdb$HORIZONS$proj.code, cmdb$HORIZONS$s.id, cmdb$HORIZONS$o.id, cmdb$HORIZONS$h.no, sep="_")
      cmdb$HORIZONS$hzn_desgn = paste(cmdb$HORIZONS$h.desig.master, cmdb$HORIZONS$h.desig.subdiv, cmdb$HORIZONS$h.desig.suffix, sep="")
      au.horT <- plyr::join_all(list(cmdb$HORIZONS[,c("h.id","s.id","h.no","h.texture","hzn_desgn","h.upper.depth","h.lower.depth")], au.hor, au.xy))
      au.horT$site_obsdate = format(as.Date(au.horT$o.date.desc, format="%d%m%Y"), "%Y-%m-%d")
      au.horT$sand_tot_psa = ifelse(is.na(au.horT$sand_tot_psa), 100-(au.horT$clay_tot_psa + au.horT$silt_tot_psa), au.horT$sand_tot_psa)
      au.horT$hzn_top = au.horT$h.upper.depth*100
      au.horT$hzn_bot = au.horT$h.lower.depth*100
      au.horT$oc_d = signif(au.horT$oc / 1000 * au.horT$db_od * 1000 * (100 - ifelse(is.na(au.horT$wpg2), 0, au.horT$wpg2))/100, 3)
      au.cols.n = c("s.id", "o.location.notes", "site_obsdate", "o.longitude.GDA94", "o.latitude.GDA94", "h.id", "h.no", "hzn_top", "hzn_bot", "hzn_desgn", "h.texture", "clay_tot_psa", "silt_tot_psa", "sand_tot_psa", "oc", "oc_d", "c_tot", "n_tot", "ph_kcl", "ph_h2o", "ph_cacl2", "cec_sum", "cec_nh4", "ecec", "wpg2", "db_od", "ca_ext", "mg_ext", "na_ext", "k_ext", "ec_satp", "ec_12pre")
      x.na = au.cols.n[which(!au.cols.n %in% names(au.horT))]
      if(length(x.na)>0){ for(i in x.na){ au.horT[,i] = NA } }
      chemsprops.NatSoil = au.horT[,au.cols.n]
      chemsprops.NatSoil$source_db = "CSIRO_NatSoil"
      chemsprops.NatSoil$confidence_degree = 4
      chemsprops.NatSoil = complete.vars(chemsprops.NatSoil, sel = c("oc","db_od","clay_tot_psa","ph_h2o"), coords = c("o.longitude.GDA94", "o.latitude.GDA94"))  
    }
    dim(chemsprops.NatSoil)

    ## [1] 89862    34

#### NAMSOTER

-   Coetzee, M. E. (2001). [NAMSOTER, a SOTER database for
    Namibia](https://edepot.wur.nl/485173). Agroecological Zoning, 458.
-   Coetzee, M. E. (2009). Chemical characterisation of the soils of
    East Central Namibia (Doctoral dissertation, Stellenbosch:
    University of Stellenbosch).

<!-- -->

    if(!exists("chemsprops.NAMSOTER")){
      nam.profs <- read.csv("/mnt/DATA/Soil_points/Namibia/NAMSOTER/Namibia_all_profiles.csv", na.strings = c("-9999", "999", "9999", "NA"))
      nam.hors <- read.csv("/mnt/DATA/Soil_points/Namibia/NAMSOTER/Namibia_all_horizons.csv", na.strings = c("-9999", "999", "9999", "NA"))
      #summary(nam.hors$TOTN)
      #summary(nam.hors$TOTC)
      nam.hors$hzn_top <- NA
      nam.hors$hzn_top <- ifelse(nam.hors$HONU==1, 0, nam.hors$hzn_top)
      h.lst <- lapply(1:7, function(x){which(nam.hors$HONU==x)})
      for(i in 2:7){
        sel <- match(nam.hors$PRID[h.lst[[i]]], nam.hors$PRID[h.lst[[i-1]]])
        nam.hors$hzn_top[h.lst[[i]]] <- nam.hors$HBDE[h.lst[[i-1]]][sel]
      }
      nam.hors$HBDE <- ifelse(is.na(nam.hors$HBDE), nam.hors$hzn_top+50, nam.hors$HBDE)
      #summary(nam.hors$HBDE)
      namALL = plyr::join(nam.hors, nam.profs, by=c("PRID"))
      namALL$k_ext = namALL$EXCK * 391
      namALL$ca_ext = namALL$EXCA * 200
      namALL$mg_ext = namALL$EXMG * 121
      namALL$na_ext = namALL$EXNA * 230
      #summary(namALL$MINA)
      namALL$BULK <- ifelse(namALL$BULK>2.4, NA, namALL$BULK)
      namALL$wpg2 = ifelse(namALL$MINA=="D", 80, ifelse(namALL$MINA=="A", 60, ifelse(namALL$MINA=="M", 25, ifelse(namALL$MINA=="C", 10, ifelse(namALL$MINA=="V", 1, ifelse(namALL$MINA=="F", 2.5, ifelse(namALL$MINA=="M/A", 40, ifelse(namALL$MINA=="C/M", 15, 0))))))))
      #hist(namALL$wpg2)
      namALL$oc_d = signif(namALL$TOTC / 1000 * namALL$BULK * 1000 * (100 - ifelse(is.na(namALL$wpg2), 0, namALL$wpg2))/100, 3)
      #summary(namALL$oc_d)
      #summary(namALL$PHAQ) ## very high ph
      namALL$site_obsdate = 2000
      nam.col = c("PRID", "SLID", "site_obsdate", "LONG", "LATI", "labsampnum", "HONU", "hzn_top", "HBDE", "HODE", "PSCL", "CLPC", "STPC", "SDTO", "TOTC", "oc_d", "c_tot", "TOTN", "PHKC", "PHAQ", "ph_cacl2", "CECS", "cec_nh4", "ecec", "wpg2", "BULK", "ca_ext", "mg_ext", "na_ext", "k_ext", "ELCO", "ec_12pre")
      x.na = nam.col[which(!nam.col %in% names(namALL))]
      if(length(x.na)>0){ for(i in x.na){ namALL[,i] = NA } }
      chemsprops.NAMSOTER = namALL[,nam.col]
      chemsprops.NAMSOTER$source_db = "NAMSOTER"
      chemsprops.NAMSOTER$confidence_degree = 2
      chemsprops.NAMSOTER = complete.vars(chemsprops.NAMSOTER, sel = c("TOTC","CLPC","PHAQ"), coords = c("LONG", "LATI"))  
    }
    dim(chemsprops.NAMSOTER)

    ## [1] 2955   34

#### Worldwide organic soil carbon and nitrogen data

-   Zinke, P. J., Millemann, R. E., & Boden, T. A. (1986). [Worldwide
    organic soil carbon and nitrogen
    data](https://cdiac.ess-dive.lbl.gov/ftp/ndp018/ndp018.pdf). Carbon
    Dioxide Information Center, Environmental Sciences Division, Oak
    Ridge National Laboratory. Data download URL:
    <a href="https://dx.doi.org/10.3334/CDIAC/lue.ndp018" class="uri">https://dx.doi.org/10.3334/CDIAC/lue.ndp018</a>

-   Note: poor spatial location accuracy i.e. &lt;10 km. Bulk density
    for many points has been estimated not measured. Sampling year has
    not been but literature indicates: 1965, 1974, 1976, 1978,
    1979, 1984. Most of samples come from natural vegetation
    (undisturbed) areas.

<!-- -->

    if(!exists("chemsprops.ISCND")){
      ndp.profs <- read.csv("/mnt/DATA/Soil_points/INT/ISCND/ndp018.csv", na.strings = c("-9999", "?", "NA"), stringsAsFactors = FALSE)
      names(ndp.profs) = c("PROFILE", "CODE", "CARBON", "NITROGEN", "LAT", "LONG", "ELEV", "SOURCE", "HOLDRIGE", "OLSON", "PARENT")
      for(j in c("CARBON","NITROGEN","ELEV")){  ndp.profs[,j] <- as.numeric(ndp.profs[,j])   }
      #summary(ndp.profs$CARBON)
      lat.s  <- grep("S", ndp.profs$LAT) # lat.n <- grep("N", ndp.profs$LAT)
      ndp.profs$latitude_decimal_degrees = as.numeric(gsub("[^0-9.-]", "", ndp.profs$LAT))
      ndp.profs$latitude_decimal_degrees[lat.s] = ndp.profs$latitude_decimal_degrees[lat.s] * -1
      lon.w <- grep("W", ndp.profs$LONG) # lon.e  <- grep("E", ndp.profs$LONG, fixed = TRUE)
      ndp.profs$longitude_decimal_degrees = as.numeric(gsub("[^0-9.-]", "", ndp.profs$LONG))
      ndp.profs$longitude_decimal_degrees[lon.w] = ndp.profs$longitude_decimal_degrees[lon.w] * -1
      #plot(ndp.profs[,c("longitude_decimal_degrees", "latitude_decimal_degrees")])
      ndp.profs$hzn_top = 0; ndp.profs$hzn_bot = 100
      ## Years: 1965, 1974, 1976, 1978, 1979, 1984 
      ndp.profs$site_obsdate = "1982"
      ndp.col = c("PROFILE", "CODE", "site_obsdate", "longitude_decimal_degrees", "latitude_decimal_degrees", "labsampnum", "layer_sequence","hzn_top","hzn_bot","hzn_desgn", "tex_psda", "clay_tot_psa", "silt_tot_psa", "sand_tot_psa", "oc", "CARBON", "c_tot", "n_tot", "ph_kcl", "ph_h2o", "ph_cacl2", "cec_sum", "cec_nh4", "ecec", "wpg2", "db_od", "ca_ext", "mg_ext", "na_ext", "k_ext", "ec_satp", "ec_12pre")
      x.na = ndp.col[which(!ndp.col %in% names(ndp.profs))]
      if(length(x.na)>0){ for(i in x.na){ ndp.profs[,i] = NA } }
      chemsprops.ISCND = ndp.profs[,ndp.col]
      chemsprops.ISCND$source_db = "ISCND"
      chemsprops.ISCND$confidence_degree = 8
      chemsprops.ISCND = complete.vars(chemsprops.ISCND, sel = c("CARBON"), coords = c("longitude_decimal_degrees", "latitude_decimal_degrees"))  
    }
    dim(chemsprops.ISCND)

    ## [1] 4114   34

#### Interior Alaska Carbon and Nitrogen stocks

-   Manies, K., Waldrop, M., and Harden, J. (2020): Generalized models
    to estimate carbon and nitrogen stocks of organic soil horizons in
    Interior Alaska, Earth Syst. Sci. Data, 12, 1745–1757,
    <a href="https://doi.org/10.5194/essd-12-1745-2020" class="uri">https://doi.org/10.5194/essd-12-1745-2020</a>,
    Data download URL:
    <a href="https://doi.org/10.5066/P960N1F9" class="uri">https://doi.org/10.5066/P960N1F9</a>

<!-- -->

    if(!exists("chemsprops.Alaska")){
      al.gps <- read.csv("/mnt/DATA/Soil_points/USA/Alaska_Interior/Site_GPS_coordinates_v1-1.csv", stringsAsFactors = FALSE)
      ## Different datums!
      #summary(as.factor(al.gps$Datum))
      al.gps1 = al.gps[al.gps$Datum=="NAD83",]
      coordinates(al.gps1) = ~ Longitude + Latitude
      proj4string(al.gps1) = "+proj=longlat +datum=NAD83"
      al.gps0 = spTransform(al.gps1, CRS("+proj=longlat +datum=WGS84"))
      al.gps[which(al.gps$Datum=="NAD83"),"Longitude"] = al.gps0@coords[,1]
      al.gps[which(al.gps$Datum=="NAD83"),"Latitude"] = al.gps0@coords[,2]
      al.gps$site = al.gps$Site
      al.hor <- read.csv("/mnt/DATA/Soil_points/USA/Alaska_Interior/Generalized_models_for_CandN_Alaska_v1-1.csv", stringsAsFactors = FALSE)
      al.hor$hzn_top = al.hor$depth - as.numeric(al.hor$thickness)
      al.hor$site_obsdate = format(as.Date(al.hor$date, format = "%m/%d/%Y"), "%Y-%m-%d")
      al.hor$oc = as.numeric(al.hor$carbon) * 10
      al.hor$n_tot = as.numeric(al.hor$nitrogen) * 10
      al.hor$oc_d = as.numeric(al.hor$Cdensity) * 1000
      #summary(al.hor$oc_d)
      al.horA = plyr::join(al.hor, al.gps, by=c("site"))
      al.col = c("profile", "description", "site_obsdate", "Longitude", "Latitude", "sampleID", "layer_sequence", "hzn_top", "depth", "Hcode", "tex_psda", "clay_tot_psa", "silt_tot_psa", "sand_tot_psa", "oc", "oc_d", "c_tot", "n_tot", "ph_kcl", "ph_h2o", "ph_cacl2", "cec_sum", "cec_nh4", "ecec", "wpg2", "BDfine", "ca_ext", "mg_ext", "na_ext", "k_ext", "ec_satp", "ec_12pre")
      x.na = al.col[which(!al.col %in% names(al.horA))]
      if(length(x.na)>0){ for(i in x.na){ al.horA[,i] = NA } }
      chemsprops.Alaska = al.horA[,al.col]
      chemsprops.Alaska$source_db = "Alaska_interior"
      chemsprops.Alaska$confidence_degree = 1
      chemsprops.Alaska = complete.vars(chemsprops.Alaska, sel = c("oc","oc_d"), coords = c("Longitude", "Latitude"))  
    }
    dim(chemsprops.Alaska)

    ## [1] 4013   34

#### Remnant native SOC database

-   Sanderman, J., (2017) “Remnant native SOC database for
    release.xlsx”, Soil carbon profile data from paired land use
    comparisons,
    <a href="https://doi.org/10.7910/DVN/QQQM8V/8MSBNI" class="uri">https://doi.org/10.7910/DVN/QQQM8V/8MSBNI</a>,
    Harvard Dataverse, V1

<!-- -->

    if(!exists("chemsprops.RemnantSOC")){
      rem.hor <- openxlsx::read.xlsx("/mnt/DATA/Soil_points/INT/WHRC_remnant_SOC/remnant+native+SOC+database+for+release.xlsx", sheet = 3)
      rem.site <- openxlsx::read.xlsx("/mnt/DATA/Soil_points/INT/WHRC_remnant_SOC/remnant+native+SOC+database+for+release.xlsx", sheet = 2)
      rem.ref <- openxlsx::read.xlsx("/mnt/DATA/Soil_points/INT/WHRC_remnant_SOC/remnant+native+SOC+database+for+release.xlsx", sheet = 4)
      rem.site = plyr::join(rem.site, rem.ref[,c("Source.No.","DOI","Sample_year")], by=c("Source.No."))
      rem.site$Site = rem.site$Site.ID
      rem.horA = plyr::join(rem.hor, rem.site, by=c("Site"))
      rem.horA$hzn_top = rem.horA$'U_depth.(m)'*100
      rem.horA$hzn_bot = rem.horA$'L_depth.(m)'*100
      rem.horA$db_od = ifelse(is.na(as.numeric(rem.horA$'measured.BD.(Mg/m3)')), as.numeric(rem.horA$'estimated.BD.(Mg/m3)'), as.numeric(rem.horA$'measured.BD.(Mg/m3)'))
      rem.horA$oc_d = signif(rem.horA$'OC.(g/kg)' * rem.horA$db_od, 3)
      #summary(rem.horA$oc_d)
      rem.col = c("Source.No.", "Site", "Sample_year", "Longitude", "Latitude", "labsampnum", "layer_sequence", "hzn_top", "hzn_bot", "hzn_desgn", "tex_psda", "clay_tot_psa", "silt_tot_psa", "sand_tot_psa", "OC.(g/kg)", "oc_d", "c_tot", "n_tot", "ph_kcl", "ph_h2o", "ph_cacl2", "cec_sum", "cec_nh4", "ecec", "wpg2", "db_od", "ca_ext", "mg_ext", "na_ext", "k_ext", "ec_satp", "ec_12pre")
      x.na = rem.col[which(!rem.col %in% names(rem.horA))]
      if(length(x.na)>0){ for(i in x.na){ rem.horA[,i] = NA } }
      chemsprops.RemnantSOC = rem.horA[,rem.col]
      chemsprops.RemnantSOC$source_db = "WHRC_remnant_SOC"
      chemsprops.RemnantSOC$confidence_degree = 8
      chemsprops.RemnantSOC = complete.vars(chemsprops.RemnantSOC, sel = c("OC.(g/kg)","oc_d"), coords = c("Longitude", "Latitude"))  
    }
    dim(chemsprops.RemnantSOC)

    ## [1] 1631   34

#### Soil Health DB

-   Jian, J., Du, X., & Stewart, R. D. (2020). A database for global
    soil health assessment. Scientific Data, 7(1), 1-8.
    <a href="https://doi.org/10.1038/s41597-020-0356-3" class="uri">https://doi.org/10.1038/s41597-020-0356-3</a>.
    Data download URL:
    <a href="https://github.com/jinshijian/SoilHealthDB" class="uri">https://github.com/jinshijian/SoilHealthDB</a>

Note: some information is available about column names
(<a href="https://www.nature.com/articles/s41597-020-0356-3/tables/3" class="uri">https://www.nature.com/articles/s41597-020-0356-3/tables/3</a>)
but detailed explanation is missing.

    if(!exists("chemsprops.SoilHealthDB")){
      shdb.hor <- openxlsx::read.xlsx("/mnt/DATA/Soil_points/INT/SoilHealthDB/SoilHealthDB_V2.xlsx", sheet = 1, na.strings = c("NA", "NotAvailable", "Not-available"))
      #summary(as.factor(shdb.hor$SamplingDepth))
      shdb.hor$hzn_top = as.numeric(sapply(shdb.hor$SamplingDepth, function(i){ strsplit(i, "-to-")[[1]][1] }))
      shdb.hor$hzn_bot = as.numeric(sapply(shdb.hor$SamplingDepth, function(i){ strsplit(i, "-to-")[[1]][2] }))
      shdb.hor$hzn_top = ifelse(is.na(shdb.hor$hzn_top), 0, shdb.hor$hzn_top)
      shdb.hor$hzn_bot = ifelse(is.na(shdb.hor$hzn_bot), 15, shdb.hor$hzn_bot)
      shdb.hor$oc = as.numeric(shdb.hor$BackgroundSOC) * 10
      shdb.hor$oc_d = signif(shdb.hor$oc * shdb.hor$SoilBD, 3)
      for(j in c("ClayPerc", "SiltPerc", "SandPerc", "SoilpH")){   shdb.hor[,j] = as.numeric(shdb.hor[,j])   }
      #summary(shdb.hor$oc_d)
      shdb.col = c("StudyID", "ExperimentID", "SamplingYear", "Longitude", "Latitude", "labsampnum", "layer_sequence", "hzn_top", "hzn_bot", "hzn_desgn", "Texture", "ClayPerc", "SiltPerc", "SandPerc", "oc", "oc_d", "c_tot", "n_tot", "ph_kcl", "SoilpH", "ph_cacl2", "cec_sum", "cec_nh4", "ecec", "wpg2", "SoilBD", "ca_ext", "mg_ext", "na_ext", "k_ext", "ec_satp", "ec_12pre")
      x.na = shdb.col[which(!shdb.col %in% names(shdb.hor))]
      if(length(x.na)>0){ for(i in x.na){ shdb.hor[,i] = NA } }
      chemsprops.SoilHealthDB = shdb.hor[,shdb.col]
      chemsprops.SoilHealthDB$source_db = "SoilHealthDB"
      chemsprops.SoilHealthDB$confidence_degree = 8
      chemsprops.SoilHealthDB = complete.vars(chemsprops.SoilHealthDB, sel = c("ClayPerc","SoilpH","oc"), coords = c("Longitude", "Latitude"))  
    }
    dim(chemsprops.SoilHealthDB)

    ## [1] 1371   34

#### Pseudo-observations

-   Pseudo-observations using simulated points (world deserts)

<!-- -->

    if(!exists("chemsprops.SIM")){
      ## 0 soil organic carbon + 98% sand content (deserts)
      load("deserts.pnt.rda")
      nut.sim <- as.data.frame(spTransform(deserts.pnt, CRS("+proj=longlat +datum=WGS84")))
      nut.sim[,1] <- NULL
      nut.sim <- plyr::rename(nut.sim, c("x"="longitude_decimal_degrees", "y"="latitude_decimal_degrees"))
      nr = nrow(nut.sim)
      nut.sim$site_key <- paste("Simulated", 1:nr, sep="_")
      ## insert zeros for all nutrients except for the once we are not sure:
      ## http://www.decodedscience.org/chemistry-sahara-sand-elements-dunes/45828
      sim.vars = c("oc", "oc_d", "c_tot", "n_tot", "ecec", "clay_tot_psa", "mg_ext", "k_ext")
      nut.sim[,sim.vars] <- 0
      nut.sim$silt_tot_psa = 2
      nut.sim$sand_tot_psa = 98
      nut.sim$hzn_top = 0
      nut.sim$hzn_bot = 30
      nut.sim$db_od = 1.55
      nut.sim2 = nut.sim
      nut.sim2$silt_tot_psa = 1
      nut.sim2$sand_tot_psa = 99
      nut.sim2$hzn_top = 30
      nut.sim2$hzn_bot = 60
      nut.sim2$db_od = 1.6
      nut.simA = rbind(nut.sim, nut.sim2)
      #str(nut.simA)
      nut.simA$source_db = "Simulated"
      nut.simA$confidence_degree = 10
      x.na = col.names[which(!col.names %in% names(nut.simA))]
      if(length(x.na)>0){ for(i in x.na){ nut.simA[,i] = NA } }
      chemsprops.SIM = nut.simA[,col.names]
    }
    dim(chemsprops.SIM)

    ## [1] 718  34

Other potential large soil profile DBs of interest:

-   Shangguan, W., Dai, Y., Liu, B., Zhu, A., Duan, Q., Wu, L., … &
    Chen, D. (2013). [A China data set of soil properties for land
    surface modeling](https://doi.org/10.1002/jame.20026). Journal of
    Advances in Modeling Earth Systems, 5(2), 212-224.

![alt text](../../../tex/R_logo.svg.png "Bind everything") Bind everything
--------------------------------------------------------------------------

    #ls(pattern=glob2rx("chemsprops.*"))
    tot_sprops = dplyr::bind_rows(lapply(ls(pattern=glob2rx("chemsprops.*")), function(i){ mutate_all(setNames(get(i), col.names), as.character) }))
    ## convert to numeric:
    for(j in c("longitude_decimal_degrees", "latitude_decimal_degrees", "layer_sequence", "hzn_top", "hzn_bot", "clay_tot_psa", "silt_tot_psa", "sand_tot_psa", "oc", "oc_d", "c_tot", "n_tot", "ph_kcl", "ph_h2o", "ph_cacl2", "cec_sum", "cec_nh4", "ecec", "wpg2", "db_od", "ca_ext", "mg_ext", "na_ext", "k_ext", "ec_satp", "ec_12pre")){
      tot_sprops[,j] = as.numeric(tot_sprops[,j])
    }

    ## Warning: NAs introduced by coercion

    ## Warning: NAs introduced by coercion

    ## Warning: NAs introduced by coercion

    ## Warning: NAs introduced by coercion

    #head(tot_sprops)
    tot_sprops$location_id = as.factor(paste("ID", round(tot_sprops$longitude_decimal_degrees,5), round(tot_sprops$latitude_decimal_degrees,5), sep="_"))
    #length(levels(as.factor(tot_sprops$location_id)))
    tot_sprops.pnts = tot_sprops[!duplicated(tot_sprops$location_id),c("site_key","source_db","longitude_decimal_degrees","latitude_decimal_degrees","location_id")]
    nrow(tot_sprops.pnts)

    ## [1] 126896

    ## 124815 unique locations
    coordinates(tot_sprops.pnts) <- ~ longitude_decimal_degrees + latitude_decimal_degrees
    proj4string(tot_sprops.pnts) <- "+init=epsg:4326"
    summary(as.factor(tot_sprops$source_db))

    ##           AfSPDB  Alaska_interior      Canada_CUFS      Canada_NPDB 
    ##            68833             4013            15873            16405 
    ##      China_SOTER            CIFOR        CostaRica    CSIRO_NatSoil 
    ##             5115             1633             2051            89862 
    ##             FEBR       GEMAS_2009        Iran_SPDB            ISCND 
    ##             8001             4131             4942             4114 
    ##       ISRIC_WISE          LandPKS       LUCAS_2009      MangrovesDB 
    ##            23420            69548            19899             7987 
    ##         NAMSOTER            NCSCD     Russia_EGRPR        Simulated 
    ##             2955             7371             4328              718 
    ##           SISLAC     SoilHealthDB        USDA_NCSS WHRC_remnant_SOC 
    ##            54732             1371           138535             1631

Clean up typos and physically impossible values:

    for(j in c("clay_tot_psa", "sand_tot_psa", "silt_tot_psa", "wpg2")){
      tot_sprops[,j] = ifelse(tot_sprops[,j]>100|tot_sprops[,j]<0, NA, tot_sprops[,j])
    }
    for(j in c("ph_h2o","ph_kcl","ph_cacl2")){
      tot_sprops[,j] = ifelse(tot_sprops[,j]>12|tot_sprops[,j]<2, NA, tot_sprops[,j])
    }
    #hist(tot_sprops$db_od)
    for(j in c("db_od")){
      tot_sprops[,j] = ifelse(tot_sprops[,j]>2.4|tot_sprops[,j]<0.05, NA, tot_sprops[,j])
    }
    #hist(tot_sprops$oc)
    for(j in c("oc")){
      tot_sprops[,j] = ifelse(tot_sprops[,j]>800|tot_sprops[,j]<0, NA, tot_sprops[,j])
    }
    tot_sprops$hzn_depth = tot_sprops$hzn_top + (tot_sprops$hzn_bot-tot_sprops$hzn_top)/2

#### Histogram plots

    library(ggplot2)
    ggplot(tot_sprops, aes(x=source_db, y=log1p(oc))) + geom_boxplot() + theme(axis.text.x = element_text(angle = 90, hjust = 1))

    ## Warning: Removed 182203 rows containing non-finite values (stat_boxplot).

![](README_files/figure-gfm/unnamed-chunk-28-1.png)<!-- -->

    ggplot(tot_sprops, aes(x=source_db, y=db_od)) + geom_boxplot() + theme(axis.text.x = element_text(angle = 90, hjust = 1))

    ## Warning: Removed 414340 rows containing non-finite values (stat_boxplot).

![](README_files/figure-gfm/unnamed-chunk-29-1.png)<!-- -->

    ggplot(tot_sprops, aes(x=source_db, y=log1p(oc_d))) + geom_boxplot() + theme(axis.text.x = element_text(angle = 90, hjust = 1))

    ## Warning in log1p(oc_d): NaNs produced

    ## Warning in log1p(oc_d): NaNs produced

    ## Warning: Removed 422534 rows containing non-finite values (stat_boxplot).

![](README_files/figure-gfm/unnamed-chunk-30-1.png)<!-- -->

    ggplot(tot_sprops, aes(x=source_db, y=ph_h2o)) + geom_boxplot() + theme(axis.text.x = element_text(angle = 90, hjust = 1))

    ## Warning: Removed 176646 rows containing non-finite values (stat_boxplot).

![](README_files/figure-gfm/unnamed-chunk-31-1.png)<!-- -->

    ggplot(tot_sprops, aes(x=source_db, y=clay_tot_psa)) + geom_boxplot() + theme(axis.text.x = element_text(angle = 90, hjust = 1))

    ## Warning: Removed 132992 rows containing non-finite values (stat_boxplot).

![](README_files/figure-gfm/unnamed-chunk-32-1.png)<!-- -->

    ggplot(tot_sprops, aes(x=source_db, y=log1p(cec_sum))) + geom_boxplot() + theme(axis.text.x = element_text(angle = 90, hjust = 1))

    ## Warning: Removed 371261 rows containing non-finite values (stat_boxplot).

![](README_files/figure-gfm/unnamed-chunk-33-1.png)<!-- -->

    ggplot(tot_sprops, aes(x=source_db, y=log1p(n_tot))) + geom_boxplot() + theme(axis.text.x = element_text(angle = 90, hjust = 1))

    ## Warning: Removed 381567 rows containing non-finite values (stat_boxplot).

![](README_files/figure-gfm/unnamed-chunk-34-1.png)<!-- -->

    ggplot(tot_sprops, aes(x=source_db, y=log1p(k_ext))) + geom_boxplot() + theme(axis.text.x = element_text(angle = 90, hjust = 1))

    ## Warning: Removed 383631 rows containing non-finite values (stat_boxplot).

![](README_files/figure-gfm/unnamed-chunk-35-1.png)<!-- -->

#### RDS files

Plot in Goode Homolozine projection and save final objects:

    if(!file.exists("../../../img/sol_chem.pnts_sites.png")){
      tot_sprops.pnts_sf <- st_as_sf(tot_sprops.pnts[1], crs=4326)
      plot_gh(tot_sprops.pnts_sf, out.pdf="../../../img/sol_chem.pnts_sites.pdf")
      ## extremely slow --- takes 15mins
      system("pdftoppm ../../../img/sol_chem.pnts_sites.pdf ../../../img/sol_chem.pnts_sites -png -f 1 -singlefile")
      system("convert -crop 1280x575+36+114 ../../../img/sol_chem.pnts_sites.png ../../../img/sol_chem.pnts_sites.png")
    }

<img src="../../../img/sol_chem.pnts_sites.png" title="Soil hydraulic and physical soil properties." alt="Soil hydraulic and physical soil properties." width="100%" />

![alt text](../../../tex/R_logo.svg.png "Overlay www.OpenLandMap.org layers") Overlay with www.OpenLandMap.org layers
---------------------------------------------------------------------------------------------------------------------

Remove points that are not allowed to be distributed:

    sel.rm = tot_sprops$source_db=="LUCAS_2009" | tot_sprops$site_key %in% mng.rm
    tot_sprops.s = tot_sprops[!sel.rm,]

Load the tiling system (1 degree grid representing global land mask) and
run spatial overlay in parallel:

    if(!exists("rm.sol")){
      tile.pol = readOGR("../../../tiles/global_tiling_100km_grid.gpkg")
      #length(tile.pol)
      ov.sol <- extract.tiled(obj=tot_sprops.pnts, tile.pol=tile.pol, path="/data/tt/LandGIS/grid250m", ID="ID", cpus=64)
      ## Valid predictors:
      pr.vars = unique(unlist(sapply(c("fapar", "landsat", "lc100", "mod09a1", "mod11a2", "alos.palsar", "sm2rain", "irradiation_solar.atlas", "usgs.ecotapestry", "floodmap.500y", "water.table.depth_deltares", "snow.prob_esacci", "water.vapor_nasa.eo", "wind.speed_terraclimate", "merit.dem_m", "merit.hydro_m", "cloud.fraction_earthenv", "water.occurance_jrc", "wetlands.cw_upmc", "pb2002"), function(i){names(ov.sol)[grep(i, names(ov.sol))]})))
      str(pr.vars)
      ## Final regression matrix:
      rm.sol = plyr::join(tot_sprops.s, ov.sol[,c("location_id", pr.vars)])
    }
    dim(rm.sol)

Save final analysis-ready objects:

    saveRDS(tot_sprops.s, "../../../out/rds/sol_chem.pnts_horizons.rds")
    saveRDS(tot_sprops, "/mnt/DATA/Soil_points/sol_chem.pnts_horizons.rds")
    library(farff)
    writeARFF(tot_sprops.s, "../../../out/arff/sol_chem.pnts_horizons.arff", overwrite = TRUE)
    ## compressed CSV
    write.csv(tot_sprops.s, file=gzfile("../../../out/csv/sol_chem.pnts_horizons.csv.gz"))
    saveRDS.gz(rm.sol, "../../../out/rds/sol_chem.pnts_horizons_rm.rds")

Save temp object:

    save.image.pigz(file="./themes/sol/SoilChemDB/soilchem.RData")
